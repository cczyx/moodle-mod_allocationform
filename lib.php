<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Library of interface functions and constants for module allocationform
 *
 * All the core Moodle functions, neeeded to allow the module to work
 * integrated in Moodle should be placed here.
 * All the allocationform specific functions, needed to implement all the module
 * logic, should go to locallib.php. This will help to save some memory when
 * Moodle is performing actions across all modules.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2012 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Returns the information on whether the module supports a feature
 *
 * @see plugin_supports() in lib/moodlelib.php
 * @param string $feature FEATURE_xx constant for requested feature
 * @return mixed true if the feature is supported, null if unknown
 *
 * FEATURE_GRADE_HAS_GRADE, FEATURE_GRADE_OUTCOMES, FEATURE_COMPLETION_TRACKS_VIEWS, FEATURE_COMPLETION_HAS_RULES,
 * FEATURE_NO_VIEW_LINK, FEATURE_IDNUMBER, FEATURE_GROUPS, FEATURE_GROUPINGS, FEATURE_GROUPMEMBERSONLY, FEATURE_MOD_ARCHETYPE,
 * FEATURE_MOD_INTRO, FEATURE_MODEDIT_DEFAULT_COMPLETION, FEATURE_COMMENT, FEATURE_RATE, FEATURE_BACKUP_MOODLE2
 */
function allocationform_supports($feature) {
    switch($feature) {
        case FEATURE_MOD_INTRO:
        case FEATURE_BACKUP_MOODLE2:
        case FEATURE_COMPLETION_TRACKS_VIEWS:
        case FEATURE_COMPLETION_HAS_RULES:
            return true;
        case FEATURE_GRADE_HAS_GRADE:
        case FEATURE_GRADE_OUTCOMES:
        case FEATURE_RATE:
        case FEATURE_COMMENT:
        case FEATURE_GROUPINGS:
        case FEATURE_GROUPS:
            return false;
        default:
            return null;
    }
}

/**
 * List of view style log actions
 * @return array
 */
function allocationform_get_view_actions() {
    return array('view', 'view all', 'generate_csv', 'access_denied');
}

/**
 * List of update style log actions
 * @return array
 */
function allocationform_get_post_actions() {
    return array('update', 'add', 'back_to_edit', 'make_ready', 'edit_option', 'edit_disallow',
        'reprocess', 'set_results_available');
}

/**
 * This function is used by the reset_course_userdata function in moodlelib.
 * @param array $data the data submitted from the reset course.
 * @return array status array
 */
function allocationform_reset_userdata($data) {
    return array();
}

/**
 * Saves a new instance of the allocationform into the database
 *
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will create a new instance and return the id number
 * of the new instance.
 *
 * @param stdClass $allocationform An object from the form in mod_form.php
 * @param mod_allocationform_mod_form $mform
 * @return int The id of the newly inserted allocationform record
 */
function allocationform_add_instance(stdClass $allocationform, mod_allocationform_mod_form $mform = null) {
    global $DB;

    $allocationform->timecreated = time();

    return $DB->insert_record('allocationform', $allocationform);
}

/**
 * Updates an instance of the allocationform in the database
 *
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will update an existing instance with new data.
 *
 * @param stdClass $allocationform An object from the form in mod_form.php
 * @param mod_allocationform_mod_form $mform
 * @return boolean Success/Fail
 */
function allocationform_update_instance(stdClass $allocationform, mod_allocationform_mod_form $mform = null) {
    global $DB;

    $allocationform->timemodified = time();
    $allocationform->id = $allocationform->instance;

    return $DB->update_record('allocationform', $allocationform);
}

/**
 * Removes an instance of the allocationform from the database
 *
 * Given an ID of an instance of this module,
 * this function will permanently delete the instance
 * and any data that depends on it.
 *
 * @param int $id Id of the module instance
 * @return boolean Success/Failure
 */
function allocationform_delete_instance($id) {
    global $DB;

    if (! $allocationform = $DB->get_record('allocationform', array('id' => $id))) {
        return false;
    }

    $DB->delete_records('allocationform_allocations', array('formid' => $allocationform->id));
    $DB->delete_records('allocationform_choices', array('formid' => $allocationform->id));
    $DB->delete_records('allocationform_disallow', array('formid' => $allocationform->id));
    $DB->delete_records('allocationform_options', array('formid' => $allocationform->id));
    $DB->delete_records('allocationform', array('id' => $allocationform->id));

    return true;
}

/**
 * Obtains the automatic completion state for this allocationform based on any conditions in allocationform settings.
 *
 * @param object $course Course
 * @param object $cm Course-module
 * @param int $userid User ID
 * @param bool $type Type of comparison (or/and; can be used as return value if no conditions)
 * @return bool True if completed, false if not. (If no conditions, then return
 *   value depends on comparison type)
 */
function allocationform_get_completion_state($course, $cm, $userid, $type) {
    global $DB;

    // Get allocationform details.
    if (!($allocationform = $DB->get_record('allocationform', array('id' => $cm->instance)))) {
        throw new Exception("Can't find allocation form ID: {$cm->instance}");
    }

    $result = $type; // Default return value.

    // If completion option is enabled, evaluate it and return true/false.
    if ($allocationform->trackcompletion) {
        $choices = $DB->record_exists('allocationform_choices', array('formid' => $cm->instance, 'userid' => $userid));

        return completion_info::aggregate_completion_states($type, $result, $choices);
    } else {
        // Completion option is not enabled so just return $type.
        return $type;
    }
}

/**
 * Generate content for the course overview.
 *
 * Display to users when:
 * - They can make choices and have not yet done so.
 * - Their allocation is available to them.
 * - To staff when they can review allocations.
 * 
 * @param mixed $courses The list of courses to print the overview for
 * @param array $htmlarray The array of html to modified.
 * @return void
 */
function allocationform_print_overview($courses, &$htmlarray) {
    global $USER, $DB, $PAGE;

    // Check that courses have been passed.
    if (empty($courses) || !is_array($courses) || count($courses) == 0) {
        return;
    }

    // Get all the allocation activities in the courses, if none present do nothing more.
    if (!$allocations = get_all_instances_in_courses('allocationform', $courses)) {
        return;
    }

    $renderer = $PAGE->get_renderer('mod_allocationform');

    // Find the allocation forms the user has submitted to.
    $params = array();
    $in = array();
    $count = 0;
    foreach ($allocations as $allocation) {
        $params['allocation'.++$count] = $allocation->id;
        $in[] = ':allocation'.$count;
    }
    $params['user'] = $USER->id;
    $where = 'userid = :user AND formid IN('.  implode(',', $in).')';
    $allocationchoices = $DB->get_fieldset_select('allocationform_choices', 'formid', $where, $params);

    $time = time();

    foreach ($allocations as $allocation) {
        $context = context_module::instance($allocation->coursemodule);
        $usercanbeallocated = user_has_role_assignment($USER->id, $allocation->roleid, $context->id);
        $message = ''; // The message to be displayed to the user.

        if ($allocation->state == mod_allocationform_helper::STATE_READY
                && $allocation->startdate < $time && $allocation->deadline > $time
                && !in_array($allocation->id, $allocationchoices) && $usercanbeallocated) {
            // The allocation is open.
            // The user can be allocated.
            // The user has not made a choice.
            $message = $renderer->render_overview($allocation,
                    get_string('overviewclose', 'mod_allocationform', array('closedate' => userdate($allocation->deadline))));
        } else if ($allocation->state == mod_allocationform_helper::STATE_PROCESSED && $allocation->deadline < $time + 2592000
                && $usercanbeallocated) {
            // The allocation is completed.
            // The user can be allocated.
            // The allocation deadline was within the last 30 days.
            $message = $renderer->render_overview($allocation, get_string('overviewready', 'mod_allocationform'));
        } else if ($allocation->state == mod_allocationform_helper::STATE_REVIEW
                && has_capability('mod/allocationform:edit', $context)) {
            // The allocation form is in a review state and the user is an editing teacher.
            $message = $renderer->render_overview($allocation, get_string('overviewreview', 'mod_allocationform'));
        }

        if (!empty($message)) {
            // If we get here a message was generated for the allocation form.
            if (!array_key_exists($allocation->course, $htmlarray)) {
                $htmlarray[$allocation->course] = array();
            }
            if (!array_key_exists('allocationform', $htmlarray[$allocation->course])) {
                $htmlarray[$allocation->course]['allocationform'] = ''; // Initialize, avoid warnings.
            }
            $htmlarray[$allocation->course]['allocationform'] .= $message;
        }
    }
}

/**
 * This is used by the recent activity block
 * 
 * Given a course and a time, this module should find recent activity
 * that has occurred in allocationform activities and print it out.
 * Return true if there was output, or false is there was none.
 * 
 * @param mixed $course the course to print activity for
 * @param bool $viewfullnames boolean to determine whether to show full names or not
 * @param int $timestart the time the rendering started
 * @return bool true if activity was printed, false otherwise.
 */
function allocationform_print_recent_activity($course, $viewfullnames, $timestart) {
    return false;  //  True if anything was printed, otherwise false.
}

/**
 * Prepares the recent activity data
 *
 * This callback function is supposed to populate the passed array with
 * custom activity records. These records are then rendered into HTML via
 * {@link allocationform_print_recent_mod_activity()}.
 *
 * @param array $activities sequentially indexed array of objects with the 'cmid' property
 * @param int $index the index in the $activities to use for the next record
 * @param int $timestart append activity since this time
 * @param int $courseid the id of the course we produce the report for
 * @param int $cmid course module id
 * @param int $userid check for a particular user's activity only, defaults to 0 (all users)
 * @param int $groupid check for a particular group's activity only, defaults to 0 (all groups)
 * @return void adds items into $activities and increases $index
 */
function allocationform_get_recent_mod_activity(&$activities, &$index, $timestart, $courseid, $cmid, $userid=0, $groupid=0) {
}

/**
 * Prints single activity item prepared by {see allocationform_get_recent_mod_activity()}
 * 
 * @param stdClass $activity the activity object the glossary resides in
 * @param int $courseid the id of the course the glossary resides in
 * @param bool $detail not used, but required for compatibilty with other modules
 * @param int $modnames not used, but required for compatibilty with other modules
 * @param bool $viewfullnames not used, but required for compatibilty with other modules
 */
function allocationform_print_recent_mod_activity($activity, $courseid, $detail, $modnames, $viewfullnames) {
}

/**
 * Returns an array of users who are participanting in this allocationform
 *
 * Must return an array of users who are participants for a given instance
 * of allocationform. Must include every user involved in the instance,
 * independient of his role (student, teacher, admin...). The returned
 * objects must contain at least id property.
 * See other modules as example.
 *
 * @param int $allocationformid ID of an instance of this module
 * @return boolean|array false if no participants, array of objects otherwise
 */
function allocationform_get_participants($allocationformid) {
    global $DB;

    $params = array();
    $params['formid'] = $allocationformid;
    $sql = "SELECT DISTINCT u.id
                    FROM {role_assignments} r
                    JOIN {user} u ON u.id = r.userid
                    LEFT JOIN {allocationform_choices} a ON u.id = a.userid
                    WHERE a.formid = :formid";

    return $DB->get_records_sql($sql, $params);
}

/**
 * Returns all other caps used in the module
 *
 * @return array
 */
function allocationform_get_extra_capabilities() {
    return array('mod/allocationform:viewform',
        'mod/allocationform:reallocate',
        'mod/allocationform:edit',
        'mod/allocationform:viewallocations');
}

// File API.

/**
 * Returns the lists of all browsable file areas within the given module context
 *
 * The file area 'intro' for the activity introduction field is added automatically
 * by {@link file_browser::get_file_info_context_module()}
 *
 * @param stdClass $course
 * @param stdClass $cm
 * @param stdClass $context
 * @return array of [(string)filearea] => (string)description
 */
function allocationform_get_file_areas($course, $cm, $context) {
    return array();
}

/**
 * Serves the files from the allocationform file areas
 *
 * @param stdClass $course
 * @param stdClass $cm
 * @param stdClass $context
 * @param string $filearea
 * @param array $args
 * @param bool $forcedownload
 * @return void this should never return to the caller
 */
function allocationform_pluginfile($course, $cm, $context, $filearea, array $args, $forcedownload) {

    if ($context->contextlevel != CONTEXT_MODULE) {
        send_file_not_found();
    }

    require_login($course, true, $cm);

    send_file_not_found();
}

/**
 * Check if the module has any update that affects the current user since a given time.
 *
 * @param cm_info $cm course module data
 * @param int $from the time to check updates from
 * @param array $filter if we need to check only specific updates
 * @return stdClass an object with the different type of areas indicating if they were updated or not
 * @since Moodle 3.2
 */
function allocationform_check_updates_since(cm_info $cm, $from, $filter = array()) {
    // It isn't really possible at present to detect any other updates in the plugin right now as nothing is dated.
    $updates = course_check_module_updates_since($cm, $from, array(), $filter);
    return $updates;
}
