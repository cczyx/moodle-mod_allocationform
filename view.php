<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Prints a particular instance of allocationform
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @copyright  2012 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once(dirname(__FILE__) . '/lib.php');
require_once($CFG->libdir . '/completionlib.php');

$id = optional_param('id', 0, PARAM_INT); // Course_module ID.
$instanceid = optional_param('n', 0, PARAM_INT);  // Allocationform instance ID.
$function = optional_param('function', 0, PARAM_INT); // Defines which function the page should do.
$optionid = optional_param('option', 0, PARAM_INT); // Defines the option.
$confirm = optional_param('confirm', 0, PARAM_BOOL); // Confirmation to delete all choices.

if ($id) {
    list($course, $cm) = get_course_and_cm_from_cmid($id, 'allocationform');
} else if ($instanceid) {
    list($course, $cm) = get_course_and_cm_from_instance($instanceid, 'allocationform');
} else {
    print_error('invalidaccess');
}

$allocationform = new mod_allocationform_init($DB->get_record('allocationform', array('id' => $cm->instance), '*', MUST_EXIST));

require_login($course, true, $cm);
$context = context_module::instance($cm->id);
$courseurl = new moodle_url('/course/view.php', array('id' => $course->id));

// Print the page header.
$PAGE->set_url('/mod/allocationform/view.php', array('id' => $cm->id));
$PAGE->set_title(format_string($allocationform->get_name()));
$PAGE->set_heading(format_string($course->fullname));
$PAGE->set_context($context);

require_capability('mod/allocationform:viewform', $context); // User does not have permission to view the allocation form.
// Tell the page to use the allocationform renderer.
$output = $PAGE->get_renderer('mod_allocationform');
$helper = new mod_allocationform_helper();
$form = new mod_allocationform_renderable($cm, $allocationform, $course, $context);

// Store the state of the form.
$formstate = $allocationform->get_state();

switch ($formstate) {
    case mod_allocationform_helper::STATE_EDITING: // The form is having it's options edited.
        if ($form->editingrights) {
            if ($optionid > 0) {
                // Create a new mod_allocationform_option_form.
                $allocationoptionform = new mod_allocationform_option_form(null,
                        array('course' => $course->id, 'allocationform' => $allocationform->get_id(), 'id' => $cm->id,
                    'option' => $optionid, 'function' => mod_allocationform_helper::FUNC_EDIT_OPTION));
                // Find the option.
                $option = $DB->get_record('allocationform_options', array('id' => $optionid), '*', MUST_EXIST);
                // Add the submitted data to form.
                $allocationoptionform->set_data($option);
                // New disallow object.
                $disallowlist = new mod_allocationform_disallow($allocationform->get_id(), $optionid);
                // Create a new disallow form.
                $disallowform = new mod_allocationform_disallow_form(null,
                        array('course' => $course->id, 'allocationform' => $allocationform->get_id(), 'id' => $cm->id,
                    'option' => $optionid, 'users' => $disallowlist->get_users()));
                $disallowlist->parse_form($disallowform);
                // Create an option object.
                $newoption = new mod_allocationform_option($option);
                if ($function == mod_allocationform_helper::FUNC_DELETE_OPTION) {// The option should be deleted.
                    $newoption->delete();
                }
            } else {
                // Create a new mod_allocationform_option_form.
                $allocationoptionform = new mod_allocationform_option_form(null,
                        array('course' => $course->id, 'allocationform' => $allocationform->get_id(), 'id' => $cm->id));

                // Create an empty option object.
                $newoption = new mod_allocationform_option(new stdClass());
            }
            if (!$newoption->parse_form($allocationoptionform)) {
                $url = new moodle_url('/mod/allocationform/view.php', array('id' => $id));
                echo $output->notice(get_string('duplicateoption', 'mod_allocationform'), $allocationform->get_name(), $url);
                break;
            }

            if ($function == mod_allocationform_helper::FUNC_ADD_OPTION
                    || $function == mod_allocationform_helper::FUNC_EDIT_OPTION || $newoption->valid !== true) {
                // The form will only show invalid if data was submitted.
                // Log that the page has been viewed.
                $event = \mod_allocationform\event\option_edited::create(array(
                    'objectid' => $allocationform->get_id(),
                    'context' => $context
                ));
                $event->trigger();
                echo $output->display_edit_form($form, $newoption);
            } else if ($function == mod_allocationform_helper::FUNC_FINISH_EDIT) {
                $url = $helper->change_state($form, mod_allocationform_helper::STATE_READY);
                redirect($url);
            } else if ($function == mod_allocationform_helper::FUNC_DISALLOW_EDIT) {
                // Log that the page has been viewed.
                $event = \mod_allocationform\event\permission_edited::create(array(
                    'objectid' => $allocationform->get_id(),
                    'context' => $context
                ));
                $event->trigger();
                echo $output->display_disallowlist($form, $disallowlist);
            } else {
                // Log that the page has been viewed.
                $event = \mod_allocationform\event\allocations_viewed::create(array(
                    'objectid' => $allocationform->get_id(),
                    'context' => $context
                ));
                $event->trigger();
                // Display all the options for the form.
                echo $output->display_options($form);
            }
        } else {
            // Log that the page has been viewed.
            $event = \mod_allocationform\event\access_denied::create(array(
                'objectid' => $allocationform->get_id(),
                'context' => $context,
                'other' => array(
                    'function' => $function,
                    'option' => $optionid,
                    'confirm' => $confirm
                        )));
            $event->trigger();
            // Show an error.
            echo $output->notice(get_string('not_active', 'mod_allocationform'),
                    $allocationform->get_name(), $courseurl, 'notifymessage');
        }
        break;

    case mod_allocationform_helper::STATE_READY: // The form will let people input their choices.
        if ($function == mod_allocationform_helper::FUNC_EDIT_STATE && $form->editingrights) { // Switch back to editing mode.
            if ($confirm) {
                $url = $helper->change_state($form, mod_allocationform_helper::STATE_EDITING);
                redirect($url);
            } else {
                echo $output->confirm($allocationform->get_name(), $cm->id);
                break;
            }
        }
        // Log that the page has been viewed.
        $event = \mod_allocationform\event\allocations_viewed::create(array(
            'objectid' => $allocationform->get_id(),
            'context' => $context
        ));
        $event->trigger();
        $now = time();

        if ($form->deadlinepassed) { // The deadline has passed, so send the form for processing.
            $url = $helper->change_state($form, mod_allocationform_helper::STATE_PROCESS);
            redirect($url);
        }

        if (user_has_role_assignment($USER->id, $allocationform->get_roleid(), $context->id)) {
            $savedata = true;
        } else {
            $savedata = false;
        }

        if ($function == mod_allocationform_helper::FUNC_CSV && $form->editingrights) {
            // Show a list of the choices users have made.
            $helper->create_choice_csv($form);
        } else if ($savedata || $form->editingrights) { // The current user has the role we want to display the form to.
            if ($allocationform->get_startdate() > $now) { // Too early.
                if ($form->editingrights) { // User has editing rights, so can edit the form again.
                    echo $output->display_back_to_edit($cm, $form);
                } else {
                    // Log that the page has been viewed.
                    $event = \mod_allocationform\event\access_denied::create(array(
                        'objectid' => $allocationform->get_id(),
                        'context' => $context
                    ));
                    $event->trigger();
                    // Show an error.
                    echo $output->notice(get_string('not_active', 'mod_allocationform'),
                            $allocationform->get_name(), $courseurl, 'notifymessage');
                }
            } else {
                $allocation = new mod_allocationform_allocation($USER->id, $allocationform->get_id());
                // Create the form.
                $allocateform = new mod_allocationform_allocate_form(null,
                        array('course' => $course->id,
                            'allocationform' => $allocationform->get_id(),
                            'id' => $cm->id,
                            'function' => mod_allocationform_helper::FUNC_SAVE_PREFS,
                            'choices' => $allocationform->get_numberofchoices(),
                            'allocation' => $allocationform->get_numberofallocations(),
                            'notwant' => $allocationform->get_notwant(),
                            'user' => $USER->id));
                // Process the form.
                $allocation->parse_form($allocateform, $savedata);
                // Mark viewed by user (if required).
                $completion = new completion_info($course);
                $completion->set_module_viewed($cm);
                // Display the form.
                echo $output->display_allocationform($form, $allocation);
            }
        } else {
            // Log that the page has been viewed.
            $event = \mod_allocationform\event\access_denied::create(array(
                'objectid' => $allocationform->get_id(),
                'context' => $context
            ));
            $event->trigger();
            // User has no rights to access the form.
            echo $output->notice(get_string('deny_access', 'mod_allocationform'),
                    $allocationform->get_name(), $courseurl);
        }
        break;

    case mod_allocationform_helper::STATE_PROCESS: // The form is waiting on cron to allocate people.
        // Log that the page has been viewed.
        $event = \mod_allocationform\event\allocations_viewed::create(array(
            'objectid' => $allocationform->get_id(),
            'context' => $context
        ));
        $event->trigger();
        // Give a message that they form is waiting to be processed.
        if ($form->reprocessrights) { // User is able to force a reallocation.
            echo $output->notice(get_string('queued_for_processing', 'mod_allocationform'),
                    $allocationform->get_name(), $courseurl, 'notifymessage');
        } else {
            echo $output->notice(get_string('queued_for_processing', 'mod_allocationform'),
                    $allocationform->get_name(), $courseurl, 'notifymessage');
        }
        break;

    case mod_allocationform_helper::STATE_REVIEW: // Waiting for an editor to review the allocations.
        if ($optionid > 0) {
            // Create a new mod_allocationform_option_form.
            $allocationoptionform = new mod_allocationform_option_form(null,
                    array('course' => $course->id, 'allocationform' => $allocationform->get_id(), 'id' => $cm->id,
                'option' => $optionid, 'function' => mod_allocationform_helper::FUNC_EDIT_OPTION));
            // Find the option.
            $option = $DB->get_record('allocationform_options', array('id' => $optionid), '*', MUST_EXIST);
            // Add the submitted data to form.
            $allocationoptionform->set_data($option);
            // Create an option object.
            $newoption = new mod_allocationform_option($option);
            if (!$newoption->parse_form($allocationoptionform)) {
                $url = new moodle_url('/mod/allocationform/view.php', array('id' => $id));
                echo $output->notice(get_string('duplicateoption', 'mod_allocationform'), $allocationform->get_name(), $url);
                break;
            }
        }
        // Log that the page has been viewed.
        $event = \mod_allocationform\event\allocations_viewed::create(array(
            'objectid' => $allocationform->get_id(),
            'context' => $context
        ));
        $event->trigger();
        if ($function == mod_allocationform_helper::FUNC_REPROCESS && $form->reprocessrights) { // Force a re-allocation.
            // Set the form to be reproccessed.
            $url = $helper->change_state($form, mod_allocationform_helper::STATE_PROCESS);
            redirect($url);
        } else if ($function == mod_allocationform_helper::FUNC_PROCESSED_STATE && $form->editingrights) {
            // Set the form to processed.
            $url = $helper->change_state($form, mod_allocationform_helper::STATE_PROCESSED);
            redirect($url);
        } else if ($function == mod_allocationform_helper::FUNC_EDIT_OPTION && $form->editingrights) {
            // Log that the page has been viewed.
            $event = \mod_allocationform\event\option_edited::create(array(
                'objectid' => $allocationform->get_id(),
                'context' => $context
            ));
            $event->trigger();
            echo $output->display_edit_form($form, $newoption);
        } else if ($function == mod_allocationform_helper::FUNC_CSV && $form->editingrights) {
            $allocationform->generate_choice_csv();
            // Log that the page has been viewed.
            $event = \mod_allocationform\event\csv_generated::create(array(
                'objectid' => $allocationform->get_id(),
                'context' => $context
            ));
            $event->trigger();
        } else if ($function == mod_allocationform_helper::FUNC_ALLOC_CSV && $form->editingrights) {
            $allocationform->generate_allocation_csv();
            // Log that the page has been viewed.
            $event = \mod_allocationform\event\csv_generated::create(array(
                'objectid' => $allocationform->get_id(),
                'context' => $context
            ));
            $event->trigger();
        } else if ($form->editingrights || $form->viewall) {
            echo $output->display_allocations($form);
        } else {
            echo $output->notice(get_string('queued_for_processing', 'mod_allocationform'),
                    $allocationform->get_name(), $courseurl, 'notifymessage');
        }
        break;

    case mod_allocationform_helper::STATE_PROCESSED:
        // The form has been allocated, and reviewed. it is now available to be viewed by anyone.
        if ($function == mod_allocationform_helper::FUNC_REPROCESS && $form->reprocessrights) {
            // Set the form to be reproccessed.
            $url = $helper->change_state($form, mod_allocationform_helper::STATE_PROCESS);
            redirect($url);
        } else if ($function == mod_allocationform_helper::FUNC_CSV && $form->editingrights) {
            $allocationform->generate_choice_csv();
            // Log that the page has been viewed.
            $event = \mod_allocationform\event\csv_generated::create(array(
                'objectid' => $allocationform->get_id(),
                'context' => $context
            ));
            $event->trigger();
        } else if ($function == mod_allocationform_helper::FUNC_ALLOC_CSV && $form->editingrights) {
            $allocationform->generate_allocation_csv();
            // Log that the page has been viewed.
            $event = \mod_allocationform\event\csv_generated::create(array(
                'objectid' => $allocationform->get_id(),
                'context' => $context
            ));
            $event->trigger();
        } else if ($form->viewall || $form->editingrights) {
            // Log that the page has been viewed.
            $event = \mod_allocationform\event\allocations_viewed::create(array(
                'objectid' => $allocationform->get_id(),
                'context' => $context
            ));
            $event->trigger();
            // Show the allocations.
            echo $output->display_allocations($form);
        } else {
            // Log that the page has been viewed.
            $event = \mod_allocationform\event\allocations_viewed::create(array(
                'objectid' => $allocationform->get_id(),
                'context' => $context
            ));
            $event->trigger();
            // Mark viewed by user (if required).
            $completion = new completion_info($course);
            $completion->set_module_viewed($cm);
            // View only your own allocation.
            echo $output->display_allocations($form, $USER->id);
        }
        break;

    default: // We should never get here unless someone has done something bad with the database.
        print_error('invalidstate', 'mod_allocationform', $courseurl);
}
