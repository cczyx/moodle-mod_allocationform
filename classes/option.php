<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File containing class that stores an option for the allocation form
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @copyright  2012 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * A class that stores an option for the allocation form
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @copyright  2012 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_allocationform_option {

    /**
     * A specific form option
     * @var object
     */
    protected $option;

    /**
     * The output of the form
     * @var string
     */
    protected $formstring;

    /**
     * Submitted form data
     * @var object
     */
    protected $formdata;

    /**
     * Whether the form has been validated
     * @var bool
     */
    public $valid = true;

    /**
     * Class constructor
     * @param stdClass $option
     */
    public function __construct(stdClass $option) {
        $this->option = $option;
    }

    /**
     * Display or handle submitted options form
     *
     * @param mod_allocationform_option_form $mform
     */
    public function parse_form(mod_allocationform_option_form $mform) {
        global $CFG;

        // Try to get the id of the allocation form page.
        $id = optional_param('id', 0, PARAM_INT);

        if ($mform->is_submitted()) { // If the form has been submitted we need to check it is valid.
            $this->valid = $mform->is_validated();
        }

        if ($mform->is_cancelled()) { // Try to redirect to the allocation form.
            $url = '';
            if (!empty($id)) {
                $url = new moodle_url($CFG->wwwroot.'/mod/allocationform/view.php', array('id' => $id));
            } else {
                $url = new moodle_url($CFG->wwwroot);
            }
            redirect($url);
        } else if ($data = $mform->get_data()) {
            $this->formdata = $data;
            return $this->update();
        } else {
            // Display the form.
            $mform->set_data($this->option);
            // Capture the output of the next part of the code.
            ob_start();
            $mform->display();
            // Put the output into a string and then wipe and close the buffer.
            $this->formstring = ob_get_contents();
            ob_end_clean();
        }
        return true;
    }

    /**
     * Returns the string representing the parsed form, if no form has been parsed it returns an empty string
     *
     * @return string
     */
    public function displayform() {
        if (!empty($this->formstring)) {
            return $this->formstring;
        }

        return '';
    }

    /**
     * Checks if a form has been submitted
     *
     * @return boolean
     */
    public function form_sumbitted() {
        if (issue_info($this->formdata) && !empty($this->formdata)) {
            return true;
        }

        return false;
    }

    /**
     * Inserts or updates an option
     *
     * @return boolean
     */
    protected function update() {
        global $DB, $COURSE;

        if (empty($this->formdata)) { // No information to update.
            $courseurl = new moodle_url('course/view.php', array('id' => $COURSE->id));
            print_error('noformdatapassed', 'mod_allocationform', $courseurl);
        }

        // Set up the object containing the database values.
        $data = new stdClass();
        if (!empty($this->formdata->heading)) {
            $data->heading = $this->formdata->heading;
        }
        $data->name = $this->formdata->name;
        $data->formid = $this->formdata->allocationform;
        $data->maxallocation = $this->formdata->maxallocation;

        if (!empty($this->option->id)) { // Update the record.
            $id = $this->option->id;
            $data->id = $id;

            try {
                $DB->update_record('allocationform_options', $data);
            } catch (dml_exception $e) {
                return false;
            }

        } else {// Add a new record.
            // Find the highest sortoder for the form and add 1 so this option is added to the end of the form.
            $params['formid'] = $this->formdata->allocationform;
            $sql = 'SELECT 1 As id, MAX(sortorder) AS sortorder FROM {allocationform_options} '.
                    'WHERE formid = :formid GROUP BY formid';
            $sortorderrecords = $DB->get_records_sql($sql, $params);
            $data->sortorder = 1;
            foreach ($sortorderrecords as $record) {
                $data->sortorder = $record->sortorder + 1;
            }

            try {
                $id = $DB->insert_record('allocationform_options', $data, true);
            } catch (dml_exception $e) {
                return false;
            }
        }

        $this->option = $DB->get_record('allocationform_options', array('id' => $id), '*', MUST_EXIST);

        return true;
    }

    /**
     * Deletes the option
     *
     * @return boolean
     */
    public function delete() {
        global $DB;

        if ($this->get_id() === false) { // No option.
            return false;
        }

        $DB->delete_records('allocationform_options', array('id' => $this->option->id));

        $this->option = new stdClass();

        return true;
    }

    /**
     * Get the option ID or false if not set
     * @return boolean||int
     */
    public function get_id() {
        if (isset($this->option->id) && !empty($this->option->id)) {
            return $this->option->id;
        }
        // It is not set so return false.
        return false;
    }

    /**
     * Get a count of restricted users
     *
     * @return boolean|int
     */
    public function count_restricted_users() {
        global $DB;

        if (isset($this->option->id) && !empty($this->option->id)) {
            return $DB->count_records('allocationform_disallow', array('disallow_allocation' => $this->option->id));
        }
        // It is not set so return false.
        return false;
    }

    /**
     * Get option name
     *
     * @return boolean|string
     */
    public function get_name() {
        if ($this->get_id() === false) { // If no id is set then this whole object is likely not correct.
            return false;
        }

        if (isset($this->option->name) && !empty($this->option->name)) {
            return $this->option->name;
        }
        // It is not set so return false.
        return false;
    }

    /**
     * Get option heading
     *
     * @return boolean|string
     */
    public function get_heading() {
        if ($this->get_id() === false) { // If no id is set then this whole object is likely not correct.
            return false;
        }

        if (isset($this->option->heading) && !empty($this->option->heading)) {
            return $this->option->heading;
        }
        // It is not set so return false.
        return false;
    }

    /**
     * Get the maximum number of allocations
     *
     * @return boolean|int
     */
    public function get_maxallocation() {
        if ($this->get_id() === false) { // If no id is set then this whole object is likely not correct.
            return false;
        }

        if (isset($this->option->maxallocation) && !empty($this->option->maxallocation)) {
            return $this->option->maxallocation;
        }
        // It is not set so return false.
        return false;
    }
    /*
     * End of data access functions
     */
}
