<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File containing the renderables for the allocation form activity module
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @copyright  2012 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Internal library of helper functions to display different states of an allocationform
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @copyright  2012 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_allocationform_renderable implements renderable {
    /**
     * Object of mod_allocationform_int class containting info about an allocation form
     * @var object
     */
    public $allocationform;
    /**
     * Moodle course object
     * @var object
     */
    public $course;
    /**
     * The user's rights to reallocate slots
     * @var bool
     */
    public $reprocessrights;
    /**
     * The user's rights to edit the allocation form
     * @var bool
     */
    public $editingrights;
    /**
     * The user's rights to view all allocations for this allocation form
     * @var bool
     */
    public $viewall;
    /**
     * The context object
     * @var object
     */
    public $context;
    /**
     * Whether there are more people to be allocated than slots
     * @var bool
     */
    public $toolittleslots = false;
    /**
     * Whether the deadline has passed.
     * @var bool
     */
    public $deadlinepassed = false;
    /**
     * Store number of slots and people
     * @var stdClass
     */
    public $stats;

    /**
     * Class constructor
     *
     * @param stdClass $cm
     * @param mod_allocationform_init $allocationform
     * @param stdClass $course
     * @param stdClass $context
     */
    public function __construct($cm, mod_allocationform_init $allocationform, $course, $context) {
        $this->cm = $cm;
        $this->allocationform = $allocationform;
        $this->course = $course;
        $this->context = $context;
        $this->reprocessrights = $this->get_reprocessingrights();
        $this->editingrights = $this->get_editingrights();
        $this->viewall = $this->get_viewall();
        $this->stats = $this->get_stats();
        $this->set_deadlinepassed();
        $this->set_slots();
    }

    /**
     *  Generate stats so an editor can see if there are enough slots in the form for all the people who can be allocated
     */
    private function get_stats() {
        $stats = new stdClass();
        if ($this->editingrights) {
            $stats->slots = $this->allocationform->get_number_of_allocation_slots();
            // Get the number of people enrolled on this course with the desired role.
            $stats->people = count_role_users($this->allocationform->get_roleid(), $this->context, true);
        }
        return $stats;
    }

    /**
     * Check whether the deadline has passed
     */
    private function set_deadlinepassed() {
        if (time() > $this->allocationform->get_deadline()) { // The deadline has passed.
            $this->deadlinepassed = true;
        }
    }

    /**
     * Get the options for this allocation form
     * @return array Allocationform options
     */
    public function get_optionrecords() {
        global $DB;
        $optionrecords = $DB->get_records('allocationform_options',
                array('formid' => $this->allocationform->get_id()), 'sortorder');

        return $optionrecords;
    }

    /**
     * Check if the user is able to edit the allocation form
     * @return boolean
     */
    private function get_reprocessingrights() {
        $reprocessrights = false;
        if (has_capability('mod/allocationform:reallocate', $this->context)) {
            $reprocessrights = true;
        }
        return $reprocessrights;
    }

    /**
     * Checks if the user has view all allocations permissions
     */
    private function get_viewall() {
        $viewall = false;
        if (has_capability('mod/allocationform:viewallocations', $this->context)) {
            $viewall = true;
        }
    }

    /**
     * Check if the user has editing rights
     * @return boolean
     */
    private function get_editingrights() {
        $editingrights = false;
        if (has_capability('mod/allocationform:edit', $this->context)) {
            $editingrights = true;
        }
        return $editingrights;
    }

    /**
     * Check whether there are more people to be allocated than slots.
     */
    private function set_slots() {
        if (property_exists($this->stats, 'people') && property_exists($this->stats, 'slots')) {
            if (($this->stats->slots / $this->allocationform->get_numberofallocations()) < $this->stats->people) {
                $this->toolittleslots = true;
            }
        }
    }
}
