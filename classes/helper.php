<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File containing helper class for the allocationform module.
 *
 * @package    mod_allocationform
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @copyright  2012 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Helper class for the allocationform module.
 *
 * @package    mod_allocationform
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @copyright  2012 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_allocationform_helper {
    // A list of states that a form can be in.
    /** Editing state */
    const STATE_EDITING = 1;
    /** Ready state */
    const STATE_READY = 2;
    /** Waiting to process state */
    const STATE_PROCESS = 4;
    /** Waiting for review */
    const STATE_REVIEW = 5;
    /** Processed state */
    const STATE_PROCESSED = 3;

    // A list of function.
    /** Add an option */
    const FUNC_ADD_OPTION = 1;
    /** Delete an option */
    const FUNC_DELETE_OPTION = 2;
    /** Edit an option */
    const FUNC_EDIT_OPTION = 3;
    /** Finish editing */
    const FUNC_FINISH_EDIT = 4;
    /** Save preferences */
    const FUNC_SAVE_PREFS = 5;
    /** Rest the form to editing state */
    const FUNC_EDIT_STATE = 6;
    /** Reprocess the allocation */
    const FUNC_REPROCESS = 7;
    /** Set to processed state */
    const FUNC_PROCESSED_STATE = 8;
    /** Edit a disallowed option */
    const FUNC_DISALLOW_EDIT = 9;
    /** Create a CSV report of choices made */
    const FUNC_CSV = 10;
    /** Generate a csv file of all users listing their allocations */
    const FUNC_ALLOC_CSV = 12;

    /** The number of times to loop during allocation. */
    const ALLOCATION_ATTEMPTS = 50000;
    /** The string 'user' */
    const USER = 'user';

    /**
     * Change the state of the allocation form
     *
     * @param mod_allocationform_renderable $form
     * @param int $state
     * @throws moodle_exception
     */
    public function change_state(mod_allocationform_renderable $form, $state) {

        $newstate = '';

        switch ($state) {
            case self::STATE_PROCESS:
                $newstate = 'reprocess';
                break;
            case self::STATE_EDITING:
                $newstate = 'editing_state';
                break;
            case self::STATE_PROCESSED:
                $newstate = 'set_results_available';
                break;
            case self::STATE_READY:
                $newstate = 'make_ready';
                break;
            case self::STATE_REVIEW:
                $newstate = 'review';
                break;
            default:
                throw new moodle_exception('invalidstate', 'mod_allocationform');
        }
        $form->allocationform->change_state($state);
        // Log that the page has been viewed.
        $event = \mod_allocationform\event\state_changed::create(array(
            'objectid' => $form->allocationform->get_id(),
            'context' => $form->context,
            'other' => array('function' => $state, 'newstate' => $newstate)
        ));
        $event->trigger();
        // Redirect back to this page.
        $url = new moodle_url('/mod/allocationform/view.php', array('id' => $form->cm->id));
        return $url;
    }

    /**
     * Create a CSV report of choices made
     * @param mod_allocationform_renderable $form
     */
    public function create_choice_csv(mod_allocationform_renderable $form) {
        $form->allocationform->generate_choice_csv();
        // Log that the page has been viewed.
        $event = \mod_allocationform\event\csv_generated::create(array(
            'objectid' => $form->allocationform->get_id(),
            'context' => $form->context
        ));
        $event->trigger();
    }
}
