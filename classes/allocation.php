<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File containing mod_allocationform_allocation class
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @copyright  2012 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

require_once("$CFG->libdir/formslib.php");

/**
 * Class that stores the choices a user has made
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @copyright  2012 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_allocationform_allocation {

    /**
     * The choices for a particular user on a particular form
     * @var object
     */
    protected $allocation;

    /**
     * The output of the form
     * @var string
     */
    protected $formstring;

    /**
     * Submitted form data
     * @var object
     */
    protected $formdata;

    /**
     * Class constructor
     * @param int $userid
     * @param int $formid
     */
    public function __construct($userid, $formid) {
        global $DB;

        // This didn't have a form id in before. This meant that there would only ever be one set of choices saved for a user! Oops!
        $this->allocation = $DB->get_record('allocationform_choices',
                array('userid' => $userid, 'formid' => $formid), '*', IGNORE_MISSING);
    }

    /**
     * Display / process choices form
     *
     * @param mod_allocationform_allocate_form $mform
     * @param boolean $savedata
     */
    public function parse_form(mod_allocationform_allocate_form $mform, $savedata = true) {
        global $CFG;

        // Try to get the id of the allocation form page.
        $id = optional_param('id', 0, PARAM_INT);

        if ($mform->is_cancelled()) { // Try to redirect to the allocation form.
            $url = '';

            if (!empty($id)) {
                $url = new moodle_url($CFG->wwwroot.'/mod/allocationform/view.php', array('id' => $id));
            } else {
                $url = new moodle_url($CFG->wwwroot);
            }

            redirect($url);

        } else if ($data = $mform->get_data()) {
            $this->formdata = $data;

            if ($savedata) {
                $this->update();
            }

            ob_start();

            if ($savedata) {
                echo html_writer::tag('p', get_string('saved', 'mod_allocationform'), array('class' => 'warning saved'));
            } else {
                echo html_writer::tag('p', get_string('notsaved', 'mod_allocationform'), array('class' => 'warning saved'));
            }

            $mform->display();
            // Put the output into a string and then wipe and close the buffer.
            $this->formstring = ob_get_contents();
            ob_end_clean();

        } else {
            // Display the form.
            $mform->set_data($this->allocation);
            // Capture the output of the next part of the code.
            ob_start();
            $mform->display();
            // Put the output into a string and then wipe and close the buffer.
            $this->formstring = ob_get_contents();
            ob_end_clean();
        }
    }

    /**
     * Create or update submitted choices
     *
     * @return boolean
     */
    protected function update() {
        global $USER, $DB, $COURSE;

        if (empty($this->formdata)) { // No information to update.
            return false;
        }

        $data = new stdClass();
        $data->formid = $this->formdata->allocationform;

        if (!empty($this->allocation->userid)) {
            $data->userid = $this->allocation->userid;
        } else {
            $data->userid = $USER->id;
        }

        if (!empty($this->formdata->notwant)) {
            $data->notwant = $this->formdata->notwant;
        }

        $choices = false;

        // Loop through the 10 possible choices and update.
        for ($i = 1; $i < 11; $i++) {
            $choice = "choice$i";
            if (!empty($this->formdata->$choice)) {
                $data->$choice = $this->formdata->$choice;
                $choices = true;
            }
        }

        if ($this->allocation) {// There was a valid record found, so do an update.
            $id = $this->allocation->id;
            $data->id = $id;
            $DB->update_record('allocationform_choices', $data);
        } else { // Create a new record.
            $id = $DB->insert_record('allocationform_choices', $data, true);
        }

        $completion = new completion_info($COURSE);
        $cm = get_coursemodule_from_instance('allocationform', $this->formdata->allocationform, $COURSE->id, false, MUST_EXIST);

        $allocationform = $DB->get_record('allocationform', array('id' => $this->formdata->allocationform));

        if ($completion->is_enabled($cm) && $allocationform->trackcompletion) {
            if ($choices || !empty($this->formdata->notwant)) {
                $completion->update_state($cm, COMPLETION_COMPLETE);
            } else {
                $completion->update_state($cm, COMPLETION_INCOMPLETE);
            }
        }

        $this->allocation = $DB->get_record('allocationform_choices', array('id' => $id), '*', MUST_EXIST);

        return true;
    }

    /**
     * Returns the string representing the parsed form, if no form has been parsed it returns an empty string
     *
     * @return string
     */
    public function displayform() {
        if (!empty($this->formstring)) {
            return $this->formstring;
        }
        return '';
    }

    /**
     * Get the allocation object for the current user
     *
     * @return object $this->allocation The allocation object retrieved from the database
     */
    public function get_allocation() {
        return $this->allocation;
    }
}
