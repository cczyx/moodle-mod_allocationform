<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File containing class that stores the restriction information
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @copyright  2012 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

/**
 * Class that stores the restriction information
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @copyright  2012 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_allocationform_disallow {
    /**
     * List of users including whether they have been disallowed from certain choices
     * @var array
     */
    protected $users;

    /**
     * The output of the disallow form
     * @var string
     */
    protected $formstring;

    /**
     * Submitted form data
     * @var object
     */
    protected $formdata;

    /**
     * Constructor method
     *
     * Sets the $this->users object which is a  list of all people eligible for allocation on the form
     * and includes any disallows for the option specified
     *
     * @param int $formid
     * @param int $optionid
     */
    public function __construct($formid, $optionid) {
        global $DB;

        $allocationform  = $DB->get_record('allocationform', array('id' => $formid), '*', MUST_EXIST);
        $course = get_course($allocationform->course);
        $cm = get_coursemodule_from_instance('allocationform', $formid, $course->id, false, MUST_EXIST);
        $context = context_module::instance($cm->id);

        // Get a list of all people eligible for allocation on the form and include any disallows for the option specified.
        $mainfields = 'id, MAX(disallowid) as disallowid, MAX(formid) as formid, ' .
                'MAX(disallow_allocation) as disallow_allocation, firstname, lastname';
        $fields = 'u.id, d.id AS disallowid, d.formid, d.disallow_allocation, u.firstname, u.lastname';
        $fields2 = 'u.id, NULL, NULL, NULL, u.firstname, u.lastname';

        $params['roleid'] = $allocationform->roleid;
        $params['optionid'] = $optionid;
        $params['context'] = $context->id;
        $params['roleid2'] = $allocationform->roleid;
        $params['optionid2'] = $optionid;
        $params['context2'] = $context->id;

        $contexts = $context->get_parent_context_ids();
        $parentcontexts = ' OR r.contextid IN ('.implode(',', $contexts).')';

        // Get all the users and indicates if they have been restricted.
        $sql = "SELECT $mainfields FROM (SELECT $fields ".
                "FROM {role_assignments} r ".
                    "JOIN {user} u ON u.id = r.userid ".
                    "LEFT JOIN {allocationform_disallow} d ON d.userid = u.id ".
                "WHERE (r.contextid = :context $parentcontexts) ".
                    "AND r.roleid = :roleid ".
                    "AND u.deleted = 0 ".
                    "AND (d.disallow_allocation = :optionid OR d.disallow_allocation IS NULL) ".
                "UNION ".
                "SELECT $fields2 ".
                "FROM {role_assignments} r ".
                    "JOIN {user} u ON u.id = r.userid ".
                    "JOIN {allocationform_disallow} d ON d.userid = u.id ".
                "WHERE (r.contextid = :context2 $parentcontexts) ".
                    "AND r.roleid = :roleid2 ".
                    "AND u.deleted = 0 ".
                    "AND NOT (d.disallow_allocation = :optionid2)) t ".
                "GROUP BY t.id , t.lastname , t.firstname ".
                "ORDER BY t.lastname ASC, t.firstname ASC";

        $this->users = $DB->get_records_sql($sql, $params);
    }

    /**
     * Processes the form
     *
     * @param mod_allocationform_disallow_form $mform
     */
    public function parse_form(mod_allocationform_disallow_form $mform) {
        global $CFG;

        // Try to get the id of the allocation form page.
        $id = optional_param('id', 0, PARAM_INT);

        if ($mform->is_cancelled()) { // Try to redirect to the allocation form.
            $url = '';
            if (!empty($id)) {
                $url = new moodle_url($CFG->wwwroot.'/mod/allocationform/view.php', array('id' => $id));
            } else {
                $url = new moodle_url($CFG->wwwroot);
            }
            redirect($url);
        } else if ($data = $mform->get_data()) {
            $this->formdata = $data;
            $this->update();
        } else {
            // Capture the output of the next part of the code.
            ob_start();
            $mform->display();
            // Put the output into a string and then wipe and close the buffer.
            $this->formstring = ob_get_contents();
            ob_end_clean();
        }
    }

    /**
     * Updates the restrictions
     */
    protected function update() {
        global $DB;

        $formid = $this->formdata->allocationform;
        $optionid = $this->formdata->option;

        // Delete all disallow entries for this option.
        $DB->delete_records('allocationform_disallow', array('formid' => $formid, 'disallow_allocation' => $optionid));

        // Find the length of the pre-pended string identifying users.
        $length = strlen(mod_allocationform_helper::USER);
        $record = new stdClass();
        $record->formid = $formid;
        $record->disallow_allocation = $optionid;

        foreach ($this->formdata as $key => $data) {
            if (substr($key, 0, $length) == mod_allocationform_helper::USER && $data == 1) {
                // This is a user who has been checked.
                // Remove the pre-appended string and get the users id.
                $record->userid = substr($key, $length);

                // Create a disallow record for the user.
                $DB->insert_record('allocationform_disallow', $record);
            }
        }
    }

    /**
     * Returns the string representing the parsed form, if no form has been parsed it returns an empty string
     *
     * @return string
     */
    public function displayform() {
        if (!empty($this->formstring)) {
            return $this->formstring;
        }
        return '';
    }

    /**
     * get_users
     *
     * @return boolean
     */
    public function get_users() {
        if (!empty($this->users)) {
            return $this->users;
        }
        return false;
    }
}