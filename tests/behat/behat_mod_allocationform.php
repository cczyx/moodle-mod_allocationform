<?php
// This file is part of the allocation form activity plugin for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Steps definitions related with the allocationform activity.
 *
 * @package   mod_allocationform
 * @copyright University of Nottingham, 2014
 * @author    Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
// NOTE: no MOODLE_INTERNAL test here, this file may be required by behat before including /config.php.

require_once(__DIR__ . '/../../../../lib/behat/behat_base.php');

use Behat\Behat\Context\Step\Given as Given,
    Behat\Gherkin\Node\TableNode as TableNode;

/**
 * Forum-related steps definitions.
 *
 * @package    mod_allocationform
 * @copyright  2014 University of Nottingham
 * @author     Barry Oosthuizen<barr.oosthuizen@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class behat_mod_allocationform extends behat_base {
    /**
     * Check that the allocation is not available
     *
     * @Then /^allocation form is not available$/
     * @return void
     */
    public function allocation_form_is_not_available() {
        $this->execute('behat_general::assert_page_contains_text', array(get_string('not_active', 'mod_allocationform')));
    }

    /**
     * Check that the allocation is available
     *
     * @Then /^allocation form is available$/
     * @return void
     */
    public function allocation_form_is_available() {
        $this->execute('behat_general::assert_page_not_contains_text', array(get_string('not_active', 'mod_allocationform')));
    }

    /**
     * Make the allocation form available
     *
     * @Given /^I make the allocation form active$/
     * @return void
     */
    public function i_make_the_allocation_form_active() {
        $this->execute('behat_general::click_link', array($this->escape(get_string('make_active', 'mod_allocationform'))));
    }

    /**
     * Adds the selected activity/resource filling the form data with the specified field/value pairs.
     * Sections 0 and 1 are also allowed on frontpage.
     *
     * @Given /^I add an allocation form to section "([^"]*)" and I fill the form with:$/
     * @param int $section
     * @param TableNode $formdata
     * @return void
     */
    public function i_add_an_allocation_form_to_section_and_i_fill_the_form_with($section, TableNode $formdata) {
        $deadline = time() + 172800;

        $day = (int)userdate($deadline, '%d');
        $month = (int)userdate($deadline, '%m');
        $year = userdate($deadline, '%Y');

        $data = array(
            array('deadline[day]', $day),
            array('deadline[month]', $month),
            array('deadline[year]', $year),
        );
        $datefields = new TableNode($data);

        $restrictiondata = array(
            array('User profile field', 'sf_country'),
            array('Method of comparison', 'doesnotcontain'),
            array('Value to compare against', 'MY'),
        );
        $restrictionfields = new TableNode($restrictiondata);
        $this->execute('behat_theme_boost_behat_course::i_add_to_section',
                array($this->escape("Allocation form"), $this->escape($section)));
        $this->execute('behat_forms::i_set_the_following_fields_to_these_values', array($formdata));
        $this->execute('behat_forms::i_set_the_following_fields_to_these_values', array($datefields));
        $this->execute('behat_forms::press_button', array('Add restriction'));
        $this->execute('behat_forms::press_button', array('User profile'));
        $this->execute('behat_forms::i_set_the_following_fields_to_these_values', array($restrictionfields));
        $this->execute('behat_forms::press_button', array(get_string('savechangesandreturntocourse')));
    }

    /**
     * Check that the allocation form is waiting to be processed
     *
     * @Then /^the allocation form is waiting to be processed$/
     * @return void
     */
    public function the_allocation_form_is_waiting_to_be_processed() {
        $this->execute('behat_general::assert_page_contains_text', array(get_string('queued_for_processing', 'mod_allocationform')));
    }

    /**
     * Check that a link to generate CSV of choices exists
     *
     * @Then /^I should see generate csv$/
     * @return void
     */
    public function i_should_see_generate_csv() {
        $this->execute('behat_general::assert_page_contains_text', array(get_string('generate_csv', 'mod_allocationform')));
    }

    /**
     * Swith the allocation form back to edit mode
     *
     * @Given /^I switch to edit mode$/
     * @return void
     */
    public function i_switch_to_edit_mode() {
        $this->execute('behat_general::click_link', array(get_string('back_to_edit', 'mod_allocationform')));
        $this->execute('behat_general::assert_page_contains_text', array(get_string('back_to_edit_warning', 'mod_allocationform')));
        $this->execute('behat_general::assert_page_contains_text', array(get_string('ok_to_continue', 'mod_allocationform')));
        $this->execute('behat_forms::press_button', array(get_string('yes')));
        $this->execute('behat_general::i_wait_to_be_redirected', array());
    }

    /**
     * Check that the duplicate option notice is displayed
     *
     * @Then /^I should see duplicate option notice$/
     * @return void
     */
    public function i_should_see_duplicate_option_notice() {
        $this->execute('behat_general::assert_page_contains_text', array(get_string('duplicateoption', 'mod_allocationform')));
    }

    /**
     * Check that the allocation form is restricted by country
     *
     * @Then /^I should see allocation form restricted by country$/
     * @return void
     */
    public function i_should_see_allocation_form_restricted_by_country() {
        $this->execute('behat_general::assert_element_contains_text',
                array(get_string('list_root_and', 'availability'), '.availabilityinfo', 'css_element'));
        $this->execute('behat_general::assert_element_contains_text', array('Country', '.availabilityinfo', 'css_element'));
        $this->execute('behat_general::assert_element_contains_text', array('MY', '.availabilityinfo', 'css_element'));
    }
    
    /**
     * Tests that a message stating the number of users and options that are affected by over restriction is present.
     *
     * @Then /^workable restrictions were exceeded for (\d+) user\(s\) and (\d+) options required per user$/
     * @return void
     */
    public function workable_restrictions_were_exceeded_for_users_options_required_per_user($users, $numberofchoices) {
        $a = new stdClass();
        $a->users = $users;
        $a->numberofchoices = $numberofchoices;
        $this->execute('behat_general::assert_page_contains_text',
                array(get_string('restrictionsexceeded', 'mod_allocationform', $a)));
    }
    
    /**
     * Deletes the named option from the current tutorial booking activity that is in it's edit stage.
     *
     * @Given /^I delete tutorial booking option "([^"]*)"$/
     * @param string $option
     * @return void
     */
    public function i_delete_tutorialbooking_option($option) {
        $xpath = "//*[contains(concat(' ', normalize-space(@class), ' '), ' option ') "
            . "and .//text()[contains(., '" . $this->escape($option) . "')]]"
            . "//a[contains(concat(' ', normalize-space(@class), ' '), ' delete ')]";
        $this->execute('behat_general::i_click_on', array($xpath, 'xpath_element'));
        $this->execute('behat_general::i_wait_to_be_redirected', array());
    }
    
    /**
     * Set the restrictions for users on  a tutorial booking option.
     *
     *  The table passed can contain two values:
     * - student (required) The username of a student
     * - status (optional) If set to 0 the user will be unchecked otherwise they will be checked
     *
     * The tutorial booking activity must be in it's editing stage.
     *
     * @Given /^I restrict tutorial booking option "([^"]*)" from:$/
     * 
     * @param string $option
     * @return void
     */
    public function i_restrict_tutorialbooking_option($option, TableNode $students) {
        global $DB;
        $xpath = "//*[contains(concat(' ', normalize-space(@class), ' '), ' option ') "
            . "and .//text()[contains(., '" . $this->escape($option) . "')]]"
            . "//a[contains(concat(' ', normalize-space(@class), ' '), ' restrict ')]";
        $this->execute('behat_general::i_click_on', array($xpath, 'xpath_element'));
        $data = array();
        foreach($students->getHash() as $row) {
            $user = $DB->get_record('user', array('username' => $row['student']), 'id');
            $field = 'user' . $user->id;
            if (isset($row['status']) && $row['status'] == '0') {
                $status = '0';
            } else {
                $status = '1';
            }
            $data[] = array($field, $status);
        }
        $datefields = new TableNode($data);
        $this->execute('behat_forms::i_set_the_following_fields_to_these_values', array($datefields));
        $this->execute('behat_forms::press_button', array(get_string('savechanges')));
        $this->execute('behat_general::i_wait_to_be_redirected', array());
    }

    /**
     * Check the restrictions for users on  a tutorial booking option.
     *
     *  The table passed can contain two values:
     * - student The username of a student
     * - status If set to 0 the user will be unchecked otherwise they will be checked
     *
     * The tutorial booking activity must be in it's editing stage.
     * 
     * @Given /^I see tutorial booking option "([^"]*)" has the following user restrictions:$/
     *
     * @global moodle_datavbase $DB
     * @param string $option
     * @param TableNode $students
     * @return void
     */
    public function i_see_users_resricted_on_tutorialbooking_option($option, TableNode $students) {
        global $DB;
        $xpath = "//*[contains(concat(' ', normalize-space(@class), ' '), ' option ') "
            . "and .//text()[contains(., '" . $this->escape($option) . "')]]"
            . "//a[contains(concat(' ', normalize-space(@class), ' '), ' restrict ')]";
        $this->execute('behat_general::i_click_on', array($xpath, 'xpath_element'));
        $data = array();
        foreach($students->getHash() as $row) {
            $user = $DB->get_record('user', array('username' => $row['student']), 'id');
            $field = 'user' . $user->id;
            if ($row['status'] == '0') {
                $status = '0';
            } else {
                $status = '1';
            }
            $data[] = array($field, $status);
        }
        $datefields = new TableNode($data);
        $this->execute('behat_forms::the_following_fields_match_these_values', array($datefields));
        $this->execute('behat_forms::press_button', array(get_string('cancel')));
        $this->execute('behat_general::i_wait_to_be_redirected', array());
    }
}
