@mod @uon @mod_allocationform @mod_allocationform_restrict
Feature: Restrict too many options from a user
    In order to warn teachers that they have restricted too many options for one or more users
    As a teacher
    I need to be see a warning if I restrict a user from too many options

Background:
    Given the following "users" exist:
        | username | firstname | lastname | email | country |
        | student1 | Sam | Student | student1@example.com | GB |
        | student2 | Sid | Student | student1@example.com | MY |
        | student3 | Seth | Student | student1@example.com | GB |
        | teacher1 | Bob | Teacher | teacher1@example.com | GB |
    And the following "courses" exist:
        | fullname | shortname | category | summary |
        | C1 | C1 | 0 | Test course |
    And the following "course enrolments" exist:
        | user | course | role |
        | student1 | C1 | student |
        | student2 | C1 | student |
        | student3 | C1 | student |
        | teacher1 | C1 | editingteacher |
    And I log in as "admin"
    And I navigate to "Advanced features" in site administration
    And I set the following fields to these values:
        | Enable restricted access | checked |
    And I press "Save changes"
    And I log out

    @javascript
    Scenario: Teachers see a warning when they restrict too many options from users
        Given I log in as "teacher1"
        And I follow "C1"
        And I turn editing mode on
        And I add an allocation form to section "1" and I fill the form with:
            | Allocation form name | Test Allocation form name |
            | Description | Test Allocation form description |
            | Number of choices | 3 |
            | Role to be allocated | Student |
        And I follow "Test Allocation form name"
        And I follow "Add new option"
        And I set the following fields to these values:
            | Option name | Apples |
            | Maximum allocations | 2 |
        And I press "Save changes"

        And I follow "Add new option"
        And I set the following fields to these values:
            | Option name | Oranges |
            | Maximum allocations | 2 |
        And I press "Save changes"

        And I follow "Add new option"
        And I set the following fields to these values:
            | Option name | Pears |
            | Maximum allocations | 2 |
        And I press "Save changes"

        And I follow "Add new option"
        And I set the following fields to these values:
            | Option name | Bananas |
            | Maximum allocations | 2 |
        And I press "Save changes"
        And I restrict tutorial booking option "Apples" from:
            | student | status |
            | student1 | 1 |
            | student2 | 0 |
            | student3 | 0 |
        And I restrict tutorial booking option "Oranges" from:
            | student | status |
            | student1 | 1 |
            | student2 | 0 |
            | student3 | 0 |
        And I restrict tutorial booking option "Pears" from:
            | student | status |
            | student1 | 1 |
            | student2 | 0 |
            | student3 | 0 |
        Then workable restrictions were exceeded for 1 user(s) and 3 options required per user

        Given I restrict tutorial booking option "Apples" from:
            | student | status |
            | student1 | 1 |
            | student2 | 1 |
            | student3 | 0 |
        And I restrict tutorial booking option "Oranges" from:
            | student | status |
            | student1 | 1 |
            | student2 | 1 |
            | student3 | 0 |
        And I restrict tutorial booking option "Pears" from:
            | student | status |
            | student1 | 1 |
            | student2 | 1 |
            | student3 | 0 |
        Then workable restrictions were exceeded for 2 user(s) and 3 options required per user

        And I see tutorial booking option "Apples" has the following user restrictions:
            | student | status |
            | student1 | 1 |
            | student2 | 1 |
            | student3 | 0 |
        And I see tutorial booking option "Oranges" has the following user restrictions:
            | student | status |
            | student1 | 1 |
            | student2 | 1 |
            | student3 | 0 |
        And I see tutorial booking option "Pears" has the following user restrictions:
            | student | status |
            | student1 | 1 |
            | student2 | 1 |
            | student3 | 0 |

        # Do the same checks on another allocation form
        Given I follow "C1"

        And I add an allocation form to section "1" and I fill the form with:
            | Allocation form name | Test Allocation form name 2 |
            | Description | Test Allocation form description 2 |
            | Number of choices | 3 |
            | Role to be allocated | Student |
        And I follow "Test Allocation form name 2"
        And I follow "Add new option"
        And I set the following fields to these values:
            | Option name | Apples |
            | Maximum allocations | 2 |
        And I press "Save changes"

        And I follow "Add new option"
        And I set the following fields to these values:
            | Option name | Oranges |
            | Maximum allocations | 2 |
        And I press "Save changes"

        And I follow "Add new option"
        And I set the following fields to these values:
            | Option name | Pears |
            | Maximum allocations | 2 |
        And I press "Save changes"

        And I follow "Add new option"
        And I set the following fields to these values:
            | Option name | Bananas |
            | Maximum allocations | 2 |
        And I press "Save changes"

        Given I restrict tutorial booking option "Apples" from:
            | student | status |
            | student1 | 1 |
            | student2 | 0 |
            | student3 | 0 |
        And I restrict tutorial booking option "Oranges" from:
            | student | status |
            | student1 | 1 |
            | student2 | 0 |
            | student3 | 0 |
        And I restrict tutorial booking option "Pears" from:
            | student | status |
            | student1 | 1 |
            | student2 | 0 |
            | student3 | 0 |
        Then workable restrictions were exceeded for 1 user(s) and 3 options required per user

        Given I restrict tutorial booking option "Apples" from:
            | student | status |
            | student1 | 1 |
            | student2 | 1 |
            | student3 | 0 |
        And I restrict tutorial booking option "Oranges" from:
            | student | status |
            | student1 | 1 |
            | student2 | 1 |
            | student3 | 0 |
        And I restrict tutorial booking option "Pears" from:
            | student | status |
            | student1 | 1 |
            | student2 | 1 |
            | student3 | 0 |
        Then workable restrictions were exceeded for 2 user(s) and 3 options required per user

        # Check that, after restrictions were made on two forms, the expected restrictions appear when you edit the restrictions.
        And I see tutorial booking option "Apples" has the following user restrictions:
            | student | status |
            | student1 | 1 |
            | student2 | 1 |
            | student3 | 0 |
        And I see tutorial booking option "Oranges" has the following user restrictions:
            | student | status |
            | student1 | 1 |
            | student2 | 1 |
            | student3 | 0 |
        And I see tutorial booking option "Pears" has the following user restrictions:
            | student | status |
            | student1 | 1 |
            | student2 | 1 |
            | student3 | 0 |
        And I see tutorial booking option "Bananas" has the following user restrictions:
            | student | status |
            | student1 | 0 |
            | student2 | 0 |
            | student3 | 0 |
