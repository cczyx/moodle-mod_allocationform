@mod @uon @mod_allocationform
Feature: Submit an allocationform

Background:
    Given the following "users" exist:
        | username | firstname | lastname | email | country |
        | student1 | Sam | Student | student1@example.com | GB |
        | student2 | Sid | Student | student1@example.com | MY |
        | student3 | Seth | Student | student1@example.com | GB |
        | teacher1 | Bob | Teacher | teacher1@example.com | GB |
    And the following "courses" exist:
        | fullname | shortname | category | summary |
        | C1 | C1 | 0 | Test course 1 |
        | C2 | C2 | 0 | Test course 2 |
    And the following "course enrolments" exist:
        | user | course | role |
        | student1 | C1 | student |
        | student2 | C1 | student |
        | student3 | C1 | student |
        | student1 | C2 | student |
        | student2 | C2 | student |
        | student3 | C2 | student |
        | teacher1 | C1 | editingteacher |
        | teacher1 | C2 | editingteacher |

    And I log in as "admin"
    And I navigate to "Advanced features" in site administration
    And I set the following fields to these values:
        | Enable restricted access | checked |
    And I press "Save changes"
    And I log out

    And I log in as "teacher1"
    And I follow "C1"
    And I turn editing mode on
    And I add an allocation form to section "1" and I fill the form with:
        | Allocation form name | Test Allocation form name |
        | Description | Test Allocation form description |
        | Number of choices | 3 |
        | Role to be allocated | Student |
    And I follow "Test Allocation form name"
    And I follow "Add new option"
    And I set the following fields to these values:
        | Option name | Apples |
        | Maximum allocations | 2 |
    And I press "Save changes"

    And I follow "Add new option"
    And I set the following fields to these values:
        | Option name | Oranges |
        | Maximum allocations | 2 |
    And I press "Save changes"

    And I follow "Add new option"
    And I set the following fields to these values:
        | Option name | Pears |
        | Maximum allocations | 2 |
    And I press "Save changes"

    And I follow "Add new option"
    And I set the following fields to these values:
        | Option name | Bananas |
        | Maximum allocations | 2 |
    And I press "Save changes"

    And I follow "Add new option"
    And I set the following fields to these values:
        | Option name | Thorns |
        | Maximum allocations | 2 |
    And I press "Save changes"

    And I follow "Add new option"
    And I set the following fields to these values:
        | Option name | Apples |
        | Maximum allocations | 2 |
    And I press "Save changes"

    Then I should see duplicate option notice

    And I am on homepage
    And I follow "C2"
    And I add an allocation form to section "1" and I fill the form with:
        | Allocation form name | Test Allocation form name2 |
        | Description | Test Allocation form description2 |
        | Number of choices | 3 |
        | Role to be allocated | Student |
    And I follow "Test Allocation form name2"
    And I follow "Add new option"

    And I set the following fields to these values:
        | Option name | Blackberries |
        | Maximum allocations | 2 |
    And I press "Save changes"
    And I follow "Add new option"

    And I set the following fields to these values:
        | Option name | Clementines |
        | Maximum allocations | 2 |
    And I press "Save changes"

    And I follow "Add new option"
    And I set the following fields to these values:
        | Option name | Avocados |
        | Maximum allocations | 2 |
    And I press "Save changes"

    And I follow "Add new option"
    And I set the following fields to these values:
      | Option name | Tomatoes |
      | Maximum allocations | 2 |
    And I press "Save changes"

    And I make the allocation form active
    And I log out

    @javascript
    Scenario: Students can make choices when an allocation form is available
        Given I log in as "student1"
        And I follow "C1"
        And I follow "Test Allocation form name"
        Then allocation form is not available
        And I log out

        Given I log in as "teacher1"
        And I follow "C1"
        And I follow "Allocation form name"
        And I delete tutorial booking option "Thorns"
        Then I should not see "Thorns"
        And I make the allocation form active
        And I log out

        Given I log in as "student1"
        And I follow "C1"
        And I follow "Test Allocation form name"
        Then allocation form is available
        And I set the following fields to these values:
          | id_choice1 | Apples |
          | id_choice2 | Oranges |
          | id_choice3 | Bananas |
        And I follow "Test Allocation form name"
        And I set the following fields to these values:
          | id_choice1 | Pears |
          | id_choice2 | Apples |
          | id_choice3 | Oranges |
        And I log out

        Given I log in as "teacher1"
        And I follow "C1"
        And I follow "Allocation form name"
        Then I should see generate csv
        Given I switch to edit mode
        And I restrict tutorial booking option "Apples" from:
            | student |
            | student1 |
        And I make the allocation form active
        And I log out

        Given I log in as "student1"
        And I follow "C1"
        And I follow "Test Allocation form name"
        Then allocation form is available
        And I should not see "Apples"

        When I am on homepage
        And I follow "C2"
        And I follow "Test Allocation form name2"
        Then allocation form is available
        And I should not see "Apples"
        And I should see "Blackberries"
        And I should see "Avocados"
        And I should see "Tomatoes"
        And I log out

        Given I log in as "student2"
        And I follow "C1"
        Then I should see allocation form restricted by country

        When I am on homepage
        And I follow "C2"
        Then I should see allocation form restricted by country
        And I log out

        Given I log in as "student3"
        And I follow "C1"
        And I follow "Test Allocation form name"
        Then allocation form is available
        And I should see "Apples"
        And I should not see "Blackberries"
        And I should not see "Avocados"
        And I should not see "Tomatoes"
        And I should not see "Thorns"
