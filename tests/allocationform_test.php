<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Unit tests
 *
 * @package   mod_allocationform
 * @copyright University of Nottingham, 2014
 * @author    Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group mod_allocationform
 * @group uon
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Test the methods used by the allocation form module.
 *
 * @package   mod_allocationform
 * @copyright University of Nottingham, 2014
 * @author    Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group mod_allocationform
 * @group uon
 */
class mod_allocationform_allocation_testcase extends advanced_testcase {
    /**
     * Test the creation of an allocation form with options and submitted choices
     */
    public function test_allocationform() {
        $this->resetAfterTest(true);

        // Test new form creation.
        $data = $this->getDataGenerator()->get_plugin_generator('mod_allocationform')->create_form();
        $this->check_form($data->form);
        $this->check_user($data->user);
        $this->check_course($data->course);

        $userid = $data->user->id;

        $context = context_course::instance($data->course->id);
        $this->assertTrue(is_enrolled($context, $data->user));
        $allocationform = new mod_allocationform_init($data->form);

        $formid = $allocationform->get_id();

        $this->check_form($allocationform->get_form());

        // Add some options and check that they were created as expected.
        $this->add_options($formid);
        $this->check_options($formid);

        // Do some more checks on the allocation form.
        $this->assertEquals(mod_allocationform_helper::STATE_READY, $allocationform->get_state());
        $this->assertEquals(35, $allocationform->get_number_of_allocation_slots());
        $this->assertEquals(1396180000, $allocationform->get_deadline());
        $this->assertEquals('Test', $allocationform->get_name());
        $this->assertEquals(1, $allocationform->get_notwant());
        $this->assertEquals(2, $allocationform->get_numberofallocations());
        $this->assertEquals(5, $allocationform->get_numberofchoices());
        $this->assertEquals($this->get_student_roleid(), $allocationform->get_roleid());
        $this->assertEquals(1396173000, $allocationform->get_startdate());
        $this->assertNotEmpty($allocationform->get_unallocated());
        $this->assertContains('Choose which slots you want', $allocationform->get_intro());

        // Add some disallowed choices and check that they were created as expected.
        $this->add_disallowed_choices($formid, $userid);
        $this->check_disallowed_choices($formid, $userid);

        // Add some submitted choices for the current user and check that they were created as expected.
        $this->setUser($userid);
        $this->add_choices($data, $formid, $userid);
        $this->check_choices($formid, $userid);

        // Process allocations and check the form and allocations for expected values.
        $this->process_allocations($formid);
        $this->check_processed_form($formid);
        $this->review_allocations($formid);
        $this->check_reviewed_form($formid);
        $this->check_allocations($formid);
    }

    /**
     * Check processed allocations for expected values
     *
     * @param int $formid
     */
    private function check_allocations($formid) {
        global $DB;

        $allocations = $DB->get_records('allocationform_allocations');

        $this->assertCount(2, $allocations);

        foreach ($allocations as $allocation) {
            $this->assertObjectHasAttribute('formid', $allocation);
            $this->assertObjectHasAttribute('userid', $allocation);
            $this->assertObjectHasAttribute('allocation', $allocation);
            $this->assertEquals($formid, $allocation->formid);
        }
    }

    /**
     * Check a processed form for expected state
     *
     * @param int $formid
     */
    private function check_processed_form($formid) {
        global $DB;

        $form = $DB->get_record('allocationform', array('id' => $formid));
        $this->assertEquals(mod_allocationform_helper::STATE_REVIEW, $form->state);
        $this->assertEquals(2, $form->numberofallocations);
        $this->assertEquals(1, $form->notwant);
        $this->assertEquals(0, $form->processed);
    }

    /**
     * Check a reviewed form for expected state
     *
     * @param int $formid
     */
    private function check_reviewed_form($formid) {
        global $DB;

        $form = $DB->get_record('allocationform', array('id' => $formid));
        $this->assertEquals(mod_allocationform_helper::STATE_PROCESSED, $form->state);
        $this->assertEquals(2, $form->numberofallocations);
        $this->assertEquals(1, $form->notwant);
        $this->assertEquals(0, $form->processed);
    }

    /**
     * Process allocations (equivalent to running the afcron.php cron script)
     *
     * @param int $formid
     */
    private function process_allocations($formid) {
        global $DB;

        // Find the allocation form do the allocation.
        $allocationform = $DB->get_record('allocationform', array('id' => $formid));
        $allocation = new mod_allocationform_init($allocationform);
        $allocation->allocate();
    }

    /**
     * Review (approved) processed allocations
     *
     * @param int $formid
     */
    private function review_allocations($formid) {
        global $DB;

        // Find the allocation form and changed the state to processed.
        $allocationform = $DB->get_record('allocationform', array('id' => $formid));
        $allocation = new mod_allocationform_init($allocationform);
        $allocation->change_state(mod_allocationform_helper::STATE_PROCESSED);
    }

    /**
     * Check that disallowed choices were created as expected
     *
     * @param int $formid
     * @param int $userid
     */
    private function check_disallowed_choices($formid, $userid) {
        global $DB;

        $disallowed = $DB->get_records('allocationform_disallow');

        $this->assertCount(1, $disallowed);

        foreach ($disallowed as $disallow) {
            $this->assertObjectHasAttribute('formid', $disallow);
            $this->assertObjectHasAttribute('userid', $disallow);
            $this->assertObjectHasAttribute('disallow_allocation', $disallow);
            $this->assertEquals($formid, $disallow->formid);
            $this->assertEquals($userid, $disallow->userid);
            $this->assertEquals(7, $disallow->disallow_allocation);
        }
    }

    /**
     * Check a user's submitted choices
     *
     * @param int $formid The allocation form object's id
     * @param int $userid The id of the user who submitted their choices
     */
    private function check_choices($formid, $userid) {
        global $DB;

        $choices = $DB->get_records('allocationform_choices', array('formid' => $formid));

        foreach ($choices as $choice) {
            $this->assertAttributeEquals($formid, 'formid', $choice);
            $this->assertAttributeEquals($userid, 'userid', $choice);
            $this->assertAttributeEquals($choice->choice1, 'choice1', $choice);
            $this->assertAttributeEquals($choice->choice2, 'choice2', $choice);
            $this->assertAttributeEquals($choice->choice3, 'choice3', $choice);
            $this->assertAttributeEquals($choice->choice4, 'choice4', $choice);
            $this->assertAttributeEquals($choice->choice5, 'choice5', $choice);
            $this->assertAttributeEquals($choice->choice6, 'choice6', $choice);
            $this->assertAttributeEquals($choice->choice7, 'choice7', $choice);
            $this->assertAttributeEquals($choice->choice8, 'choice8', $choice);
            $this->assertAttributeEquals($choice->choice9, 'choice9', $choice);
            $this->assertAttributeEquals($choice->choice10, 'choice10', $choice);
            $this->assertAttributeEquals($choice->notwant, 'notwant', $choice);
        }
    }

    /**
     * Check the allocation form's options
     *
     * @param int $formid
     */
    private function check_options($formid) {
        global $DB;

        $options = $DB->get_records('allocationform_options');

        $this->assertCount(7, $options);

        foreach ($options as $option) {
            $this->assertObjectHasAttribute('formid', $option);
            $this->assertObjectHasAttribute('name', $option);
            $this->assertObjectHasAttribute('maxallocation', $option);
            $this->assertObjectHasAttribute('heading', $option);
            $this->assertObjectHasAttribute('sortorder', $option);
            $this->assertEquals($formid, $option->formid);
            $this->assertEquals(5, $option->maxallocation);
            $this->assertEmpty($option->heading);
            $this->assertTrue(is_int(clean_param($option->sortorder, PARAM_INT)));
            $this->assertTrue(is_string(clean_param($option->name, PARAM_TEXT)));
            $this->assertContains('option', $option->name);
        }
    }

    /**
     * Check the allocation form for expected attributes
     *
     * @param object $form
     */
    private function check_form($form) {
        $this->assertObjectHasAttribute('course', $form);
        $this->assertObjectHasAttribute('name', $form);
        $this->assertObjectHasAttribute('intro', $form);
        $this->assertObjectHasAttribute('introformat', $form);
        $this->assertObjectHasAttribute('timecreated', $form);
        $this->assertObjectHasAttribute('timemodified', $form);
        $this->assertObjectHasAttribute('numberofchoices', $form);
        $this->assertObjectHasAttribute('notwant', $form);
        $this->assertObjectHasAttribute('numberofallocations', $form);
        $this->assertObjectHasAttribute('startdate', $form);
        $this->assertObjectHasAttribute('deadline', $form);
        $this->assertObjectHasAttribute('processed', $form);
        $this->assertObjectHasAttribute('state', $form);
        $this->assertObjectHasAttribute('roleid', $form);
    }

    /**
     * Check that the created user object contains the expected attributes
     *
     * @param object $user
     */
    private function check_user($user) {
        $this->assertObjectHasAttribute('username', $user);
        $this->assertObjectHasAttribute('firstname', $user);
        $this->assertObjectHasAttribute('lastname', $user);
        $this->assertObjectHasAttribute('email', $user);
    }

    /**
     * Check that the created course contains some expected attributes
     *
     * @param object $course
     */
    private function check_course($course) {
        $this->assertObjectHasAttribute('category', $course);
        $this->assertObjectHasAttribute('fullname', $course);
        $this->assertObjectHasAttribute('shortname', $course);
        $this->assertObjectHasAttribute('summary', $course);
        $this->assertObjectHasAttribute('format', $course);
    }

    /**
     * Add options to the allocation form
     *
     * @param int $formid
     */
    private function add_options($formid) {
        for ($i = 1; $i <= 7; $i++) {
            // Replace the mod_allocationform_allocate_form object with a test double (Stub the form object).
            $allocateform = $this->createMock('mod_allocationform_option_form');

            // We don't need the form to display anything so override the definition of the form to return nothing.
            $allocateform->expects($this->any())->method('definition')->will($this->returnValue(''));

            // We don't need to validate an actual form submission so override the validation of the form to return an empty array.
            $allocateform->expects($this->any())->method('validation')->will($this->returnValue(array()));

            // We don't need to test the cancellatino of the form so return false.
            $allocateform->expects($this->any())->method('is_cancelled')->will($this->returnValue(false));

            // We don't need the form to display anything so override the display method to return nothing.
            $allocateform->expects($this->any())->method('display')->will($this->returnValue(''));

            // Create fake form submission data and use stubbing / mocking to set it to the get_data() method.
            $formdata = new stdClass();
            $formdata->allocationform = $formid;
            $formdata->name = 'option_' . $i;
            $formdata->maxallocation = 5;
            $formdata->sortorder = $i;

            $allocateform->expects($this->any())->method('get_data')->will($this->returnValue($formdata));

            $newoption = new mod_allocationform_option(new stdClass());

            // Process the form.
            $newoption->parse_form($allocateform);
        }
    }

    /**
     * Add user choices to an allocation form
     *
     * @param stdClass $data
     * @param int $formid
     * @param int $userid
     */
    private function add_choices($data, $formid, $userid) {
        // Replace the mod_allocationform_allocate_form object with a test double (Stub the form object).
        $allocateform = $this->createMock('mod_allocationform_allocate_form');

        // We don't need the form to display anything so override the definition of the form to return nothing.
        $allocateform->expects($this->any())->method('definition')->will($this->returnValue(''));

        // We don't need to validate an actual form submission so override the validation of the form to return an empty array.
        $allocateform->expects($this->any())->method('validation')->will($this->returnValue(array()));

        // We don't need to test the cancellatino of the form so return false.
        $allocateform->expects($this->any())->method('is_cancelled')->will($this->returnValue(false));

        // We don't need the form to display anything so override the display method to return nothing.
        $allocateform->expects($this->any())->method('display')->will($this->returnValue(''));

        // Create fake form submission data and use stubbing / mocking to set it to the get_data() method.
        $formdata = new stdClass();
        $formdata->course = $data->course->id;
        $formdata->formid = $formid;
        $formdata->allocationform = $formid;
        $formdata->userid = $userid;
        $formdata->choice1 = 1;
        $formdata->choice2 = 2;
        $formdata->choice3 = 3;
        $formdata->choice4 = 4;
        $formdata->choice5 = 5;
        $formdata->notwant = 6;

        $allocateform->expects($this->any())->method('get_data')->will($this->returnValue($formdata));

        $allocation = new mod_allocationform_allocation($userid, $formid);

        // Process the form.
        $allocation->parse_form($allocateform);
    }

    /**
     * Add disallowed choices via a mock object
     *
     * @param int $formid
     * @param int $userid
     */
    private function add_disallowed_choices($formid, $userid) {
        // Replace the mod_allocationform_disallow_form object with a test double (Stub the form object).
        $disallowform = $this->createMock('mod_allocationform_disallow_form');

        // We don't need the form to display anything so override the definition of the form to return nothing.
        $disallowform->expects($this->any())->method('definition')->will($this->returnValue(''));

        // We don't need to validate an actual form submission so override the validation of the form to return an empty array.
        $disallowform->expects($this->any())->method('validation')->will($this->returnValue(array()));

        // We don't need to test the cancellation of the form so return false.
        $disallowform->expects($this->any())->method('is_cancelled')->will($this->returnValue(false));

        // We don't need the form to display anything so override the display method to return nothing.
        $disallowform->expects($this->any())->method('display')->will($this->returnValue(''));

        // Create fake form submission data and use stubbing / mocking to set it to the get_data() method.
        $formdata = new stdClass();
        $formdata->allocationform = $formid;
        $user = "user$userid";
        $formdata->$user = 1;
        $formdata->option = 7;

        $disallowform->expects($this->any())->method('get_data')->will($this->returnValue($formdata));

        $allocation = new mod_allocationform_disallow($formid, 7);

        // Process the form.
        $allocation->parse_form($disallowform);
    }

    /**
     * Get the first found role id for a 'student' role archetype
     *
     * @return int $roleid
     */
    private function get_student_roleid() {
        $studentroles = get_archetype_roles('student');

        foreach ($studentroles as $role) {
            $roleid = $role->id;
            break;
        }
        return $roleid;
    }
}
