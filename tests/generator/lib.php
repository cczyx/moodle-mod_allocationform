<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Allocation form data generator
 *
 * @package   mod_allocationform
 * @copyright University of Nottingham, 2014
 * @author    Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Used to generate data for mod_allocationform.
 *
 * @package   mod_allocationform
 * @copyright University of Nottingham, 2014
 * @author    Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_allocationform_generator extends testing_module_generator {
    /**
     * Creates an instance of the module for testing purposes.
     *
     * Module type will be taken from the class name. Each module type may overwrite
     * this function to add other default values used by it.
     *
     * @param array|stdClass $record Data for module being generated.
     * @param array $options General options for course module.
     * @return stdClass Record from module-defined table with additional field cmid (corresponding id in course_modules table)
     */
    public function create_instance($record = null, array $options = null) {
        $record = (object)(array)$record;

        if (!isset($record->numberofchoices)) {
            $record->numberofchoices = 0;
        }
        if (!isset($record->notwant)) {
            $record->notwant = 0;
        }
        if (!isset($record->numberofallocations)) {
            $record->numberofallocations = 0;
        }
        if (!isset($record->startdate)) {
            $record->startdate = time();
        }
        if (!isset($record->deadline)) {
            $record->deadline = time() + 86200;
        }
        if (!isset($record->processed)) {
            $record->processed = 0;
        }
        if (!isset($record->state)) {
            $record->state = mod_allocationform_helper::STATE_EDITING;
        }
        if (!isset($record->roleid)) {
            $studentroles = get_archetype_roles('student');

            foreach ($studentroles as $role) {
                $roleid = $role->id;
                break;
            }
            $record->roleid = $roleid;
        }
        return parent::create_instance($record, (array)$options);
    }

    /**
     * Create an instance of an allocation form
     *
     * @return object $data
     */
    public function create_form() {
        global $DB, $PAGE;

        require_once(dirname(dirname(dirname(__FILE__))).'/lib.php');

        $course = advanced_testcase::getDataGenerator()->create_course(array(
            'fullname' => "Test (MM1DM1)",
            'shortname' => "MM1DM1-CN-FEB1314"
        ));

        $PAGE->set_course($course);

        $now = 1396173000;

        $studentroles = get_archetype_roles('student');

        foreach ($studentroles as $role) {
            $roleid = $role->id;
            break;
        }

        $user = advanced_testcase::getDataGenerator()->create_user();
        advanced_testcase::getDataGenerator()->enrol_user($user->id, $course->id, $roleid);

        $record = new stdClass();
        $record->course = $course->id;
        $record->name = 'Test';
        $record->intro = 'Choose which slots you want';
        $record->introformat = 1;
        $record->modified = $now;
        $record->numberofchoices = 5;
        $record->notwant = 1;
        $record->numberofallocations = 2;
        $record->startdate = $now;
        $record->deadline = 1396180000;
        $record->state = mod_allocationform_helper::STATE_READY;
        $record->roleid = $roleid;

        $form = $this->create_instance($record);

        $data = new stdClass();
        $data->form = $form;
        $data->user = $user;
        $data->course = $course;
        return $data;
    }
}
