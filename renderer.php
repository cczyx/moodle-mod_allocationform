<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File containing renderers for the allocationform module
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @copyright  2012 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

/**
 * Defines the renderer for the allocationform module.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @copyright  2012 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_allocationform_renderer extends plugin_renderer_base {

    /**
     * Display the five states and highlight the current state as a progress bar
     * @param mod_allocationform_renderable $form
     * @return string The HTML containing state information
     */
    public function display_progress_bar(mod_allocationform_renderable $form) {
        $output = '';
        if ($form->editingrights) {
            $state = $form->allocationform->get_state();
            $attributes = $this->get_state_highlighting($state);
            $addstate = html_writer::tag('li', get_string('editingmode', 'mod_allocationform') . ' ►',
                    $attributes[mod_allocationform_helper::STATE_EDITING]);
            $finishstate = html_writer::tag('li', get_string('readymode', 'mod_allocationform') . ' ►',
                    $attributes[mod_allocationform_helper::STATE_READY]);
            $reviewstate = html_writer::tag('li', get_string('reviewmode', 'mod_allocationform') . ' ►',
                    $attributes[mod_allocationform_helper::STATE_PROCESS]);
            $processstate = html_writer::tag('li', get_string('processmode', 'mod_allocationform') . ' ►',
                    $attributes[mod_allocationform_helper::STATE_REVIEW]);
            $processedstate = html_writer::tag('li', get_string('processedmode', 'mod_allocationform'),
                    $attributes[mod_allocationform_helper::STATE_PROCESSED]);
            $output .= html_writer::start_tag('div', array('id' => 'mod_allocationform_state'));
            $output .= html_writer::start_tag('ol');
            $output .= $addstate . $finishstate . $processstate . $reviewstate . $processedstate;
            $output .= html_writer::end_tag('ol');
            $output .= html_writer::end_tag('div');
        }
        return $output;
    }

    /**
     * Get an array of highlighting information for different states of the allocation form
     * @param int $state
     * @return string
     */
    private function get_state_highlighting($state) {
        $attributes = array();
        $attributes[mod_allocationform_helper::STATE_EDITING] = array('class' => 'dimmed_text');
        $attributes[mod_allocationform_helper::STATE_READY] = array('class' => 'dimmed_text');
        $attributes[mod_allocationform_helper::STATE_PROCESS] = array('class' => 'dimmed_text');
        $attributes[mod_allocationform_helper::STATE_REVIEW] = array('class' => 'dimmed_text');
        $attributes[mod_allocationform_helper::STATE_PROCESSED] = array('class' => 'dimmed_text');
        $attributes[$state] = array('class' => 'bold');
        return $attributes;
    }

    /**
     * Display allocation form options
     *
     * @param mod_allocationform_renderable $form
     * @return string
     */
    public function display_options(mod_allocationform_renderable $form) {
        $output = $this->allocationform_header($form);
        // Give some information about the form.
        $output .= html_writer::start_div('', array('id' => 'mod_allocationform_info'));
        $table = new html_table();
        $table->head = array(
            get_string('slots', 'mod_allocationform'),
            get_string('numberofallocations', 'mod_allocationform'),
            get_string('people', 'mod_allocationform'),
            get_string('deadline', 'mod_allocationform'));
        $table->id = 'mod_allocationform_options';
        $table->attributes = array('class' => 'generaltable generalbox mod_introbox');
        $deadline = userdate($form->allocationform->get_deadline());
        $row1 = array($form->stats->slots, $form->allocationform->get_numberofallocations(), $form->stats->people, $deadline);
        $table->data = array($row1);
        $output .= html_writer::table($table);
        if ($form->deadlinepassed) { // The deadline has passed.
            $output .= html_writer::tag('p', get_string('deadline_passed', 'mod_allocationform'), array('class' => 'notifymessage'));
        }
        if ($form->toolittleslots) {
            // There are more people to be allocated than slots.
            $output .= html_writer::tag('p', get_string('not_enough_slots', 'mod_allocationform'), array('class' => 'notifyproblem'));
        }

        $output .= html_writer::end_div();

        // Find all the options for this form.
        $output .= html_writer::start_div();
        $output .= $this->heading(get_string('option_list', 'mod_allocationform'));
        // Display all the options.
        $options = $form->get_optionrecords();

        $restrictions = $form->allocationform->count_bad_restrictions(count($options));

        if ($restrictions) {
            $a = new stdClass;
            $a->users = $restrictions;
            $a->numberofchoices = $form->allocationform->get_numberofchoices();
            $output .= html_writer::tag('p', get_string('restrictionsexceeded', 'mod_allocationform', $a),
                    array('class' => 'warning red bold'));
        }

        $table = new html_table();
        $table->head = $this->render_option_table_header();
        foreach ($options as $record) {
            $option = new mod_allocationform_option($record);
            $table->data[] = $this->render_option($form, $option);
        }
        $output .= html_writer::table($table);
        $output .= html_writer::start_tag('p');
        // Add a link to add a new option.
        $url = new moodle_url('/mod/allocationform/view.php',
                array('id' => $form->cm->id, 'function' => mod_allocationform_helper::FUNC_ADD_OPTION));
        $output .= html_writer::link($url, get_string('add_option', 'mod_allocationform'));
        $output .= '&nbsp;&nbsp;&nbsp;&nbsp;';
        // Add a link to make the form active for allocation.
        $output .= $this->display_make_active($form);
        $output .= html_writer::end_tag('p');
        $output .= html_writer::end_div();
        $output .= $this->footer();
        return $output;
    }

    /**
     * Display an action link which will make the form active (available to students)
     * @param mod_allocationform_renderable $form
     * @return string
     */
    public function display_make_active(mod_allocationform_renderable $form) {
        // Add a link to make the form active for allocation.
        $url = new moodle_url('/mod/allocationform/view.php',
                array('id' => $form->cm->id, 'function' => mod_allocationform_helper::FUNC_FINISH_EDIT));
        $output = html_writer::link($url, get_string('make_active', 'mod_allocationform'));
        return $output;
    }

    /**
     * Display the allocation form header
     *
     * @param mod_allocationform_renderable $form
     * @return string
     */
    public function allocationform_header(mod_allocationform_renderable $form) {
        $output = $this->header();
        $output .= $this->heading($form->allocationform->get_name());
        $output .= $this->display_progress_bar($form);

        if ($form->allocationform->get_intro()) { // Conditions to show the intro can change to look for own settings or whatever.
            $output .= $this->box(format_module_intro('allocationform', $form->allocationform->get_form(), $form->cm->id),
                    'generalbox mod_introbox', 'allocationformintro');
        }
        return $output;
    }

    /**
     * Renders the edit form
     *
     * @param mod_allocationform_renderable $form
     * @param mod_allocationform_option $option
     * @return string
     */
    public function display_edit_form(mod_allocationform_renderable $form, mod_allocationform_option $option) {
        $output = $this->allocationform_header($form);
        $output .= html_writer::start_div();
        $output .= $this->heading(get_string('option_form_header', 'mod_allocationform'));
        $output .= $option->displayform();
        $output .= html_writer::end_div();
        $output .= $this->footer();
        return $output;
    }

    /**
     * Display choices allocated to slots
     *
     * @param mod_allocationform_renderable $form
     * @param object|null $user
     * @return string
     */
    public function display_allocations(mod_allocationform_renderable $form, $user = null) {
        $output = '';
        $output .= $this->allocationform_header($form);
        $output .= html_writer::tag('h3', get_string('allocations', 'mod_allocationform'));
        if ($form->allocationform->get_state() == mod_allocationform_helper::STATE_REVIEW) {
            $output .= html_writer::tag('p',
                    get_string('allocations_not_visible', 'mod_allocationform'), array('class' => 'warning'));
        }
        $rs = $form->allocationform->get_allocations($user);
        $option = null;
        if ($rs->valid()) { // There are records.
            foreach ($rs as $record) {
                if ($option !== $record->allocation) { // A new option.
                    if ($option != null) { // If not the first option close the tag.
                        $output .= html_writer::end_tag('ul');
                        $output .= html_writer::end_div();
                    }
                    // Write a header and start a list.
                    $header = $record->name;
                    if ($user === null) {
                        $header .= " ($record->maxallocation)";
                    }
                    if ($form->editingrights && $form->allocationform->get_state() == mod_allocationform_helper::STATE_REVIEW) {
                        $url = new moodle_url('/mod/allocationform/view.php', array('id' => $form->cm->id,
                            'function' => mod_allocationform_helper::FUNC_EDIT_OPTION, 'option' => $record->allocation));
                        $header .= '&nbsp;&nbsp&nbsp;'.html_writer::link($url, get_string('edit'), array('class' => 'small'));
                    }
                    $output .= html_writer::start_div('box generalbox mod_introbox allocation_box');
                    $output .= html_writer::tag('h4', $header);
                    $output .= html_writer::start_tag('ul');
                    // Update the option so we know where we are.
                    $option = $record->allocation;
                }
                if (!empty($record->lastname)) {
                    $output .= html_writer::tag('li', "$record->lastname, $record->firstname");
                }
            }
            if ($option !== null) { // Some options have been output, so we need to close up html tags.
                $output .= html_writer::end_tag('ul');
                $output .= html_writer::end_div();
            }
        } else { // No records found.
            $text = html_writer::tag('p', get_string('no_allocations', 'mod_allocationform'));
            $output .= html_writer::div($text, 'box generalbox mod_introbox');
        }
        $rs->close();
        if ($form->editingrights) {
            $output .= $this->csv_link($form->cm);
            $output .= $this->allocation_csv_link($form->cm);
        }

        if ($form->reprocessrights) {
            // Display anyone who is unallocated.
            $rs = $form->allocationform->get_unallocated();
            if ($rs->valid()) { // There are records.
                $output .= html_writer::start_div('box generalbox mod_introbox');
                $output .= html_writer::tag('h4', get_string('unallocated', 'mod_allocationform'));
                $output .= html_writer::start_tag('ul');
                foreach ($rs as $record) {
                    $output .= html_writer::tag('li', "$record->lastname, $record->firstname");
                }
                $output .= html_writer::end_tag('ul');
                $output .= html_writer::end_div();
            }
            $rs->close();
            // Display the force reallocation link.
            $output .= $this->reprocess($form->cm);
        }
        if ($form->allocationform->get_state() == mod_allocationform_helper::STATE_REVIEW) {
            $output .= $this->show_results_link($form->cm);
        }
        $output .= $this->footer();
        return $output;
    }

    /**
     * Render an allocation form option
     *
     * @param mod_allocationform_renderable $form
     * @param mod_allocationform_option $option
     * @return string
     */
    public function render_option(mod_allocationform_renderable $form, mod_allocationform_option $option) {
        $id = $option->get_id();
        $optionrow = new html_table_row();
        $optionrow->id = "option-$id";
        $optionrow->attributes = array('class' => 'option');
        $optionrow->cells[] = new html_table_cell($option->get_name(), array('class' => 'option_name'));
        $optionrow->cells[] = new html_table_cell($option->get_maxallocation(), array('class' => 'option_max'));
        $optionrow->cells[] = new html_table_cell($option->count_restricted_users(), array('class' => 'option_restricted'));

        // Create an edit link.
        $optionrow->cells[] = new html_table_cell(
            $this->render_action_link($option->get_id(), $form->cm->id, mod_allocationform_helper::FUNC_EDIT_OPTION, get_string('edit')) . '&nbsp;&nbsp;&nbsp;&nbsp;' .
            $this->render_action_link($option->get_id(), $form->cm->id, mod_allocationform_helper::FUNC_DELETE_OPTION, get_string('delete')) . '&nbsp;&nbsp;&nbsp;&nbsp;' .
            $this->render_action_link($option->get_id(), $form->cm->id, mod_allocationform_helper::FUNC_DISALLOW_EDIT, get_string('restrict', 'mod_allocationform'))
        );

        // Enclose everything in a div.
        return $optionrow;
    }

    /**
     * Generate the table header row for an option list
     * @return string
     */
    public function render_option_table_header() {
        $headerrow = array();
        $headerrow[] = new html_table_cell(get_string('option_name', 'mod_allocationform'));
        $headerrow[] = new html_table_cell(get_string('option_maxallocation', 'mod_allocationform'));
        $headerrow[] = new html_table_cell(get_string('option_restricted', 'mod_allocationform'));
        $headerrow[] = new html_table_cell(get_string('action'));
        return $headerrow;
    }

    /**
     * Render an edit, delete or select link
     *
     * @param int $optionid
     * @param int $coursemodule
     * @param int $function
     * @param string $string
     * @return string
     */
    public function render_action_link($optionid, $coursemodule, $function, $string) {
        $url = new moodle_url('/mod/allocationform/view.php',
                array('id' => $coursemodule,
                    'function' => $function,
                    'option' => $optionid));
        return html_writer::link($url, $string, array('class' => strtolower($string)));
    }

    /**
     * Generate html for the course overview.
     *
     * @param stdClass $allocation Allocation form instance generated by get_all_instances_in_courses()
     * @see get_all_instances_in_courses
     * @param string $message The message to be displayed to the user.
     * @return string
     */
    public function render_overview(stdClass $allocation, $message) {
        $url = new moodle_url('/mod/allocationform/view.php', array('id' => $allocation->coursemodule));
        $link = html_writer::link($url, $allocation->name);
        $output = html_writer::div(get_string('overviewname', 'mod_allocationform', array('link' => $link)), 'name');
        $output .= html_writer::div($message, 'info');
        return html_writer::div($output, 'allocation overview');
    }

    /**
     * Display the allocation form
     *
     * @param mod_allocationform_renderable $form
     * @param mod_allocationform_allocation $allocation
     * @return string
     */
    public function display_allocationform(mod_allocationform_renderable $form, mod_allocationform_allocation $allocation) {
        $output = '';
        $output .= $this->allocationform_header($form);

        // Display the forms deadline.
        $deadline = userdate($form->allocationform->get_deadline());
        $text = html_writer::div(get_string('deadline_message', 'mod_allocationform', array('deadline' => $deadline)));
        $output .= html_writer::tag('p', $text);

        $output .= $allocation->displayform();

        if ($form->editingrights) {
            $output .= $this->back_to_edit($form->cm);
            $output .= $this->csv_link($form->cm);
        }

        $output .= $this->footer();
        return $output;
    }

    /**
     * Dsiplay the footer
     *
     * @return string
     */
    public function footer() {
        $output = $this->back_to_course_link();
        $output .= parent::footer();
        return $output;
    }

    /**
     * Display the list of disallowed user choices
     *
     * @param mod_allocationform_renderable $form
     * @param mod_allocationform_disallow $disallow
     * @return string
     */
    public function display_disallowlist(mod_allocationform_renderable $form, mod_allocationform_disallow $disallow) {
        $output = '';
        $output .= $this->allocationform_header($form);
        $output .= $disallow->displayform();
        $output .= $this->footer();
        return $output;
    }

    /**
     * Display a 'Back to course' link
     *
     * @return object
     */
    public function back_to_course_link() {
        global $COURSE;

        $url = new moodle_url('/course/view.php', array('id' => $COURSE->id));
        $link = html_writer::link($url, get_string('show_course', 'mod_allocationform',
                array('coursename' => $COURSE->fullname)));
        $output = html_writer::div($link);
        return $output;
    }

    /**
     * Display a 'Show results' link
     *
     * @param object $cm
     * @return string
     */
    public function show_results_link($cm) {
        $url = new moodle_url('/mod/allocationform/view.php',
                array('id' => $cm->id, 'function' => mod_allocationform_helper::FUNC_PROCESSED_STATE));
        $link = html_writer::link($url, get_string('show_allocations', 'mod_allocationform'));
        $output = html_writer::div($link);
        return $output;
    }

    /**
     * Display a 'Back to edit stage' link
     *
     * @param object $cm
     * @return string
     */
    public function back_to_edit($cm) {
        $url = new moodle_url('/mod/allocationform/view.php',
                array('id' => $cm->id, 'function' => mod_allocationform_helper::FUNC_EDIT_STATE));
        $link = html_writer::link($url, get_string('back_to_edit', 'mod_allocationform'),
                array('id' => 'allocationform_back_to_edit'));
        $output = html_writer::div($link.' - '.get_string('back_to_edit_warning', 'mod_allocationform'));
        return $output;
    }

    /**
     * Display back to edit page
     * @param object $cm
     * @param mod_allocationform_renderable $allocationform
     * @return string
     */
    public function display_back_to_edit($cm, mod_allocationform_renderable $allocationform) {
        $return = $this->allocationform_header($allocationform, $cm);
        $return .= html_writer::tag('p', get_string('before_start', 'mod_allocationform'));
        $return .= $this->back_to_edit($cm);
        $return .= $this->footer();
        return $return;
    }

    /**
     * Display a link to  Generate csv of user choices
     *
     * @param object $cm
     * @return string
     */
    public function csv_link($cm) {
        $url = new moodle_url('/mod/allocationform/view.php',
                array('id' => $cm->id, 'function' => mod_allocationform_helper::FUNC_CSV));
        $link = html_writer::link($url, get_string('generate_csv', 'mod_allocationform'));
        $output = html_writer::div($link);
        return $output;
    }

    /**
     * Display a link to Generate csv of user allocations
     *
     * @param object $cm
     * @return string
     */
    public function allocation_csv_link($cm) {
        $url = new moodle_url('/mod/allocationform/view.php',
                array('id' => $cm->id, 'function' => mod_allocationform_helper::FUNC_ALLOC_CSV));
        $link = html_writer::link($url, get_string('generate_allocation_csv', 'mod_allocationform'));
        $output = html_writer::div($link);
        return $output;
    }

    /**
     * Display a link to Force reallocation
     *
     * @param object $cm
     * @return string
     */
    public function reprocess($cm) {
        $url = new moodle_url('/mod/allocationform/view.php',
                array('id' => $cm->id, 'function' => mod_allocationform_helper::FUNC_REPROCESS));
        $link = html_writer::link($url, get_string('reprocess', 'mod_allocationform'));
        $output = html_writer::div($link.' - '.get_string('reprocess_warning', 'mod_allocationform'));
        return $output;
    }

    /**
     * Render a notice message with continue button wrapped in a header and footer
     *
     * @param string $message - The message to be displayed to the user
     * @param string $form
     * @param string $link - URL to the page the user should be returned to
     * @param string $type
     * @return string
     */
    public function notice($message, $form, $link='', $type = 'notifyproblem') {
        $message = clean_text($message);

        $output = $this->header();
        $output .= $this->heading($form);
        $output .= $this->notification($message, $type);
        $output .= $this->continue_button($link);
        $output .= $this->footer();
        return $output;
    }

    /**
     * Ask the user to confirm switching to edit mode which will delete all information submitted to the form.
     *
     * @param string $form
     * @param int $id
     * @return string
     */
    public function confirm($form, $id) {
        $output = $this->header();
        $output .= $this->heading($form);
        $yesurl = new moodle_url('/mod/allocationform/view.php',
                array('confirm' => 1, 'id' => $id, 'function' => mod_allocationform_helper::FUNC_EDIT_STATE));
        $cancelurl = new moodle_url('/mod/allocationform/view.php', array('id' => $id));
        $formcontinue = new single_button($yesurl, get_string('yes'));
        $formcancel = new single_button($cancelurl, get_string('no'));
        $output .= $this->output->confirm(get_string('back_to_edit_warning', 'mod_allocationform') . ' ' .
                get_string('ok_to_continue', 'mod_allocationform'), $formcontinue, $formcancel);
        $output .= $this->footer();
        return $output;
    }
}
